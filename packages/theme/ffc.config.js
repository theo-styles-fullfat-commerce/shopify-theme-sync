const path = require('path')
const ESLintPlugin = require('eslint-webpack-plugin');

module.exports = {
  ignore: ['**/config/settings_data.json'],
  assets: {
    prettier: true,
    presets: [
      ({ config }) => {
        config.plugins.push(new ESLintPlugin({
          extensions: [`js`, `jsx`],
          exclude: [
            `node_modules`
          ],
          failOnError: true,
          emitWarning: true
        }))
      }
    ]
  },
  themes: [
    {
      id: '132230283438',
      password: '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',
      store: 'theo-test-store.myshopify.com'
    },
    {
      id: '130331902151',
      password: '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',
      store: 'student-discount-example.myshopify.com',
      deployOnly: true,
      code: "US"
    }
  ]
}