import { create } from 'evx';
import 'element-closest';
import Queue from './lib/Queue';
import Helpers from './lib/Helpers';

const { detect } = require('detect-browser');

import KeenSlider from './components/KeenSlider';
import MegaMenu from './components/MegaMenu';
import Slider from './components/Slider';
import ClassChange from './components/ClassChange';
import SlideToggle from './components/SlideToggle';
import SlideToggleGroup from './components/SlideToggleGroup';
import AddToCart from './components/AddToCart';
import Product from './components/Product';
import NumberSelector from './components/NumberSelector';
import Cart from './components/Cart';
import ObjectFit from './components/ObjectFit';
import Quicklink from './components/Quicklink';
import LazyLoad from './components/LazyLoad';
import LazyLoadTrigger from './components/LazyLoadTrigger';
import DynamicScroller from './components/DynamicScroller';
import RangeSlider from './components/RangeSlider';
import NewsletterBar from './components/NewsletterBar';
import FocusOn from './components/FocusOn';
import BoxBuilder from './components/BoxBuilder';
import CopyLink from './components/CopyLink';
import Parallax from './components/Parallax';
import KlaviyoForm from './components/KlaviyoForm';
import CountdownTimer from './components/CountdownTimer';
import StickyATC from './components/StickyATC';
import StoreLocator from './components/StoreLocator';
import StickyElem from './components/StickyElem';
import SetTimeToday from './components/SetTimeToday';
import BackgroundVideo from './components/BackgroundVideo';
import AjaxSearch from './components/AjaxSearch';

const components = {
  'lazy-load': LazyLoad,
  'object-fit': ObjectFit,
  quicklink: Quicklink,
  slider: Slider,
  'mega-menus': MegaMenu,
  'lazy-load-trigger': LazyLoadTrigger,
  'dynamic-scroller': DynamicScroller,
  'range-slider': RangeSlider,
  'keen-slider': KeenSlider,
  'class-change': ClassChange,
  'newsletter-bar': NewsletterBar,
  'slide-toggle': SlideToggle,
  'toggle-group': SlideToggleGroup,
  'number-selector': NumberSelector,
  product: Product,
  'add-to-cart': AddToCart,
  'countdown-timer': CountdownTimer,
  'focus-on': FocusOn,
  'box-builder': BoxBuilder,
  'copy-link': CopyLink,
  parallax: Parallax,
  'klaviyo-form': KlaviyoForm,
  cart: Cart,
  'sticky-atc': StickyATC,
  'store-locator': StoreLocator,
  sticky: StickyElem,
  'set-time-today': SetTimeToday,
  'background-video': BackgroundVideo,
  'ajax-search': AjaxSearch
};

const options = {
  turbolinks: false
};

const state = {
  eventQueue: new Queue(),
  ajaxCache: []
};

export default class Theme {
  constructor(ctx = state, passedOptions = {}) {
    this._options = { ...passedOptions, ...options };
    let themeInfoElem = document.querySelector('[data-theme-information]');
    if (themeInfoElem) {
      ctx = { ...ctx, ...JSON.parse(themeInfoElem.innerHTML) };
    }
    this._ctx = create(ctx);
    this._viewportEvent();
    this._bodyScrollListen();
    this._runHooks();
    this._browserDetect();
    this._components = [];
  }

  _browserDetect() {
    const browser = detect();
    if (browser) {
      document.body.classList.add(`browser--${Helpers.handleize(browser.name)}`);
      document.body.classList.add(`os--${Helpers.handleize(browser.os)}`);
    }
  }

  _viewportEvent() {
    window.addEventListener(
      'resize',
      () => {
        this._viewportLogic();
      },
      { passive: true }
    );
    this._viewportLogic();
  }

  _bodyScrollListen() {
    const targetNode = document.body;
    const config = { attributes: true, childList: false, subtree: false };

    const callback = function(mutationsList, observer) {
      document.documentElement.style.setProperty(
        '--scroll-gap-right',
        getComputedStyle(document.body).paddingRight
      );
    };

    const observer = new MutationObserver(callback);
    observer.observe(targetNode, config);
  }

  _viewportLogic() {
    let vh = window.innerHeight * 0.01;
    let vw = document.body.clientWidth * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
    document.documentElement.style.setProperty('--vw', `${vw}px`);
  }

  mountComponents(container = document) {
    for (let componentKey in components) {
      if (components.hasOwnProperty(componentKey)) {
        let selector = `[data-${componentKey}]`;
        let elements = container.querySelectorAll(selector);
        for (let elem of elements) {
          let optionStr = elem.getAttribute(`data-${componentKey}`).trim();
          let options = {};
          if (optionStr.startsWith('{') || optionStr.startsWith('[')) {
            options = JSON.parse(optionStr);
          }
          let compInstance = new components[componentKey](elem, this, options, this._ctx);
          this._components.push({
            type: componentKey,
            elem: elem,
            id: compInstance._options.id,
            component: compInstance
          });
          compInstance.mount();
        }
      }
    }
    window.__Theme = this;
    window.__ThemeComponents = this._components;

    this._ctx.emit('ffc--mounted');
  }

  unmountComponents(container = document) {
    for (var i = this._components.length - 1; i >= 0; i--) {
      let component = this._components[i];

      if (container.contains(component.elem)) {
        let componentObj = component.component;
        if (typeof componentObj.unmount === 'function') {
          componentObj.unmount();
        }
        component = null;
        componentObj = null;
        this._components.splice(i, 1);
      }
    }
  }

  getOptions() {
    return this._options;
  }

  getComponent(id) {
    return this._components.filter(component => component.id === id)[0];
  }

  getComponentByElement(elem) {
    return this._components.find(component => component.elem === elem);
  }

  _runHooks() {
    this._ctx.on('cart-item-added', state => {
      if (window.pintrk) {
        pintrk('track', 'addtocart', {
          value: state.lastItemAdded.item.price / 100.0,
          order_quantity: state.lastItemAdded.quantity
        });
      }
      if (window.fbq) {
        fbq('track', 'AddToCart');
      }
    });
  }
}
