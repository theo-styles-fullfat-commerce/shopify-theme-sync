import axios from 'axios';
import Helpers from './Helpers';

let endpoints = {
  cart: {
    clear: '/cart/clear.json',
    get: '/cart.json',
    add: '/cart/add.js',
    change: '/cart/change.js',
    update: '/cart/update.js'
  },
  product: {
    get: '/products/[handle].json',
    getCustom: '/products/[handle]?view=json'
  }
};

export default class AjaxApi {
  static async getCart() {
    let result = await axios.get(Helpers.getEndpoint(endpoints.cart.get));
    return result.data;
  }

  static async addToCart(options) {
    let result = await axios.post(Helpers.getEndpoint(endpoints.cart.add), options);
    return result.data;
  }

  static async changeCart(options) {
    let result = await axios.post(Helpers.getEndpoint(endpoints.cart.change), options);
    return result.data;
  }

  static async updateCart(options) {
    let result = await axios.post(Helpers.getEndpoint(endpoints.cart.update), options);
    return result.data;
  }

  static async emptyCart(options) {
    let result = await axios.post(Helpers.getEndpoint(endpoints.cart.clear), options);
    return result.data;
  }

  static async updateCartItem(line, quantity, props = {}) {
    return await AjaxApi.changeCart({
      line: `${line}`,
      quantity: quantity,
      properties: props
    });
  }

  static async removeCartItemById(id) {
    return await AjaxApi.changeCart({
      id: `${id}`,
      quantity: 0
    });
  }

  static async removeCartItem(id) {
    return await AjaxApi.updateCartItem(id, 0);
  }

  static async getProduct(handle) {
    let endpoint = endpoints.product.get.replace('[handle]', handle);
    let result = await axios.get(Helpers.getEndpoint(endpoint));
    return result.data.product;
  }

  static async getCustomProduct(handle) {
    let endpoint = endpoints.product.getCustom.replace('[handle]', handle);
    let result = await axios.get(Helpers.getEndpoint(endpoint), {
      responseType: 'json'
    });
    return typeof result.data === 'object' ? result.data : JSON.parse(result.data);
  }
}
