class Queue {
  constructor() {
    this.queue = [];
    this.processing = false;
  }

  onInit() {}

  add(job) {
    this.queue.push(job);
    if (!this.processing) {
      this.process();
    }
  }

  async process() {
    this.processing = true;
    if (this.queue.length === 0) {
      this.processing = false;
      return false;
    } else {
      let job = this.queue.shift();
      await job();
      this.process();
    }
  }
}

export default Queue;
