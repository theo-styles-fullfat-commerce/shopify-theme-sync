export default class Helpers {
  static formatWithDelimiters(number, precision, thousands, decimal) {
    precision = precision || 2;
    thousands = thousands || ',';
    decimal = decimal || '.';

    if (isNaN(number) || number == null) {
      return 0;
    }

    number = (number / 100.0).toFixed(precision);

    const parts = number.split('.');
    const dollarsAmount = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1' + thousands);
    const centsAmount = parts[1] ? decimal + parts[1] : '';

    return dollarsAmount + centsAmount;
  }

  static isInViewport(elem) {
    var distance = elem.getBoundingClientRect();
    return (
      distance.top >= 0 &&
      distance.bottom <= (window.innerHeight || document.documentElement.clientHeight)
    );
  }

  static hasParentWithSelector(element, selector) {
    if (element.matches(selector)) return element;
    return element.parentElement && Helpers.hasParentWithSelector(element.parentElement, selector);
  }

  static debounce(callback, wait) {
    let timeout;
    return (...args) => {
      const context = this;
      clearTimeout(timeout);
      timeout = setTimeout(() => callback.apply(context, args), wait);
    };
  }

  static getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  static compact(array) {
    var index = -1;
    var length = array == null ? 0 : array.length;
    var resIndex = 0;
    var result = [];
    while (++index < length) {
      var value = array[index];
      if (value) {
        result[resIndex++] = value;
      }
    }
    return result;
  }

  static isAnyPartOfElementInViewport(el) {
    const rect = el.getBoundingClientRect();
    // DOMRect { x: 8, y: 8, width: 100, height: 100, top: 8, right: 108, bottom: 108, left: 8 }
    const windowHeight = window.innerHeight || document.documentElement.clientHeight;
    const windowWidth = window.innerWidth || document.documentElement.clientWidth;

    // http://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
    const vertInView = rect.top <= windowHeight && rect.top + rect.height >= 0;
    const horInView = rect.left <= windowWidth && rect.left + rect.width >= 0;

    return vertInView && horInView;
  }

  static formatMoney(cents, format = '${{amount}}') {
    if (typeof cents === 'string') {
      cents = cents.replace('.', '');
    }

    let value = '';
    const placeholderRegex = /\{\{\s*(\w+)\s*\}\}/;

    switch (format.match(placeholderRegex)[1]) {
      case 'amount':
        value = Helpers.formatWithDelimiters(cents, 2);
        break;
      case 'amount_no_decimals':
        value = Helpers.formatWithDelimiters(cents, 0);
        break;
      case 'amount_with_space_separator':
        value = Helpers.formatWithDelimiters(cents, 2, ' ', '.');
        break;
      case 'amount_no_decimals_with_comma_separator':
        value = Helpers.formatWithDelimiters(cents, 0, ',', '.');
        break;
      case 'amount_no_decimals_with_space_separator':
        value = Helpers.formatWithDelimiters(cents, 0, ' ');
        break;
    }

    return format.replace(placeholderRegex, value).replace('.00', '');
  }

  static getSizedImageUrl(src, size) {
    if (size === null || src === null) {
      return src;
    }

    if (size === 'master') {
      return Helpers.removeProtocol(src);
    }

    var match = src.match(/\.(jpg|jpeg|gif|png|bmp|bitmap|tiff|tif)(\?v=\d+)?$/i);

    if (match) {
      var prefix = src.split(match[0]);
      var suffix = match[0];

      return Helpers.removeProtocol(prefix[0] + '_' + size + suffix);
    } else {
      return null;
    }
  }

  static removeProtocol(path) {
    return path.replace(/http(s)?:/, '');
  }

  static getEndpoint(endpoint) {
    if (endpoint.includes('?')) {
      return `${endpoint}&v=${Math.random()}`;
    } else {
      return `${endpoint}?v=${Math.random()}`;
    }
  }

  static nextFrame(callback) {
    window.requestAnimationFrame(() => {
      window.requestAnimationFrame(callback);
    });
  }

  static slideDown(elem, mainElem) {
    elem.style.maxHeight = 'none';
    let scrollHeight = elem.scrollHeight;
    elem.style.maxHeight = '0px';
    mainElem.classList.add('open');
    elem.classList.add('open');
    elem.dataset.transitioning = true;

    let openEvent = e => {
      if (e.propertyName === 'max-height') {
        elem.dataset.transitioning = false;
        elem.style.maxHeight = 'none';
        elem.removeEventListener('transitionend', openEvent, false);
      }
    };
    elem.addEventListener('transitionend', openEvent, false);
    Helpers.nextFrame(() => {
      elem.style.maxHeight = `${scrollHeight}px`;
    });
  }

  static slideUp(elem, mainElem) {
    elem.style.maxHeight = `${elem.scrollHeight}px`;
    mainElem.classList.remove('open');
    elem.dataset.transitioning = true;

    let closeEvent = e => {
      if (e.propertyName === 'max-height') {
        elem.classList.remove('open');
        elem.dataset.transitioning = false;
        elem.removeEventListener('transitionend', closeEvent, false);
      }
    };
    elem.addEventListener('transitionend', closeEvent, false);
    Helpers.nextFrame(() => {
      elem.style.maxHeight = `0px`;
    });
  }

  static handleize(str) {
    str = str.toLowerCase();

    var toReplace = ['"', "'", '\\', '(', ')', '[', ']'];

    // For the old browsers
    for (var i = 0; i < toReplace.length; ++i) {
      str = str.replace(toReplace[i], '');
    }

    str = str.replace(/\W+/g, '-');

    if (str.charAt(str.length - 1) == '-') {
      str = str.replace(/-+\z/, '');
    }

    if (str.charAt(0) == '-') {
      str = str.replace(/\A-+/, '');
    }

    return str;
  }

  static copyToClipboard(str) {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  }
}
