import Component from '../inherited/Component';

export default class Parallax extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._options = { ...{}, ...this._options };
    this.simpleParallax = null;
  }

  async mount() {
    const { default: SimpleParallax } = await import('simple-parallax-js');
    this._simpleParallax = new SimpleParallax(this._elem, this._options);
  }
}
