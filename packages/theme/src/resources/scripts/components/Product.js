import Component from '../inherited/Component';
import Helpers from '../lib/Helpers';

export default class Product extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._options = {
      ...{
        historyState: true
      },
      ...this._options
    };
    this._product = this._elem.querySelector('[data-product-json]')
      ? JSON.parse(this._elem.querySelector('[data-product-json]').innerHTML)
      : null;
    this._variantOptionTriggers = this._elem.querySelectorAll('[data-variant-option-trigger]');
    this._variant = null;
    this._masterSelects = this._elem.querySelectorAll('[data-master-select]');
    this._priceWrappers = this._elem.querySelectorAll('[data-product-prices]');
    this._prices = this._elem.querySelectorAll('[data-price]');
    this._priceVaries = this._elem.querySelectorAll('[data-price-varies]');
    this._comparePrices = this._elem.querySelectorAll('[data-compare-price]');
    this._quantitySelects = this._elem.querySelectorAll('[data-quantity]');
    this._images = this._elem.querySelectorAll('[data-product-image]');
    this._addToCarts = this._elem.querySelectorAll('[data-submit]');
    this._addToCartHTML = Array.from(this._addToCarts).map(elem => elem.innerHTML);

    this._buyTypeTrigger = this._elem.querySelectorAll('[data-buy-type-trigger]');

    this._subscriptionChoicesContainer = this._elem.querySelector('[data-subscription-choices]');
    this._subscriptionChoices = this._elem.querySelectorAll('[data-subscription-choice-trigger]');

    this._perElems = this._elem.querySelectorAll('[data-per-price]');

    this._quantityMultiplyElems = this._elem.querySelectorAll('[data-quantity-multiply]');

    this._extraProductSettings = this._elem.querySelector('[data-extra-product-json]')
      ? JSON.parse(this._elem.querySelector('[data-extra-product-json]').innerHTML)
      : null;
  }

  mount() {
    if (this._product === null) {
      return false;
    }

    console.log(this._product);

    this._variant = this._getVariantFromOptions();

    for (let quantity of this._quantitySelects) {
      quantity.addEventListener('change', e => {
        for (let quantityMultiply of this._quantityMultiplyElems) {
          if (quantityMultiply.hasAttribute('value')) {
            quantityMultiply.value = quantity.value * quantityMultiply.dataset.quantityMultiply;
          } else {
            quantityMultiply.innerHTML = quantity.value * quantityMultiply.dataset.quantityMultiply;
          }
        }
      });
    }

    for (let trigger of this._buyTypeTrigger) {
      trigger.addEventListener('change', e => {
        e.preventDefault();

        if (trigger.checked) {
          if (trigger.value == 'Subscribe') {
            this._subscriptionChoicesContainer.classList.add('shown');
            for (let choice of this._subscriptionChoices) {
              choice.checked = false;
              choice.dispatchEvent(new Event('change'));
            }
            for (let choice of this._subscriptionChoices) {
              choice.checked = true;
              choice.dispatchEvent(new Event('change'));
              break;
            }
          } else {
            this._sellingPlan = null;
            this._subscriptionChoicesContainer.classList.remove('shown');
            for (let choice of this._subscriptionChoices) {
              choice.checked = false;
              choice.dispatchEvent(new Event('change'));
            }

            this._onSelectChange();
          }
        }
      });
    }

    for (let choice of this._subscriptionChoices) {
      if (choice.checked) {
        this._sellingPlan = choice.value;
      }

      choice.addEventListener('change', e => {
        e.preventDefault();

        if (choice.checked) {
          this._sellingPlan = choice.value;
        }

        this._onSelectChange();
      });
    }

    for (let optionSelector of this._variantOptionTriggers) {
      optionSelector.addEventListener('change', e => {
        this._onSelectChange();
      });
    }

    this._onSelectChange();
  }

  _linkedOptions(variant) {
    for (let optionSelector of this._variantOptionTriggers) {
      optionSelector.removeAttribute('disabled');
      for (let prodVariant of this._product.variants) {
        if (
          prodVariant.option1 === variant.option1 &&
          optionSelector.value === prodVariant.option2 &&
          !prodVariant.available
        ) {
          optionSelector.setAttribute('disabled', 'disabled');
        }
      }
    }
  }

  _onSelectChange() {
    let variant = this._getVariantFromOptions();

    this._updateAddToCartState(variant);
    if (!variant) {
      return;
    }

    this._updateMasterSelect(variant);
    this._updateProductPrices(variant);
    this._switchToVariantImage(variant);
    // this._linkedOptions(variant)
    this._variant = variant;

    if (this._options.historyState) {
      this._updateHistoryState(variant);
    }
  }

  _updateHistoryState(variant) {
    if (!history.replaceState || !variant) {
      return;
    }
    let newurl = `${window.location.protocol}//${window.location.host}${window.location.pathname}?variant=${variant.id}`;

    if (this._sellingPlan) {
      newurl += `&selling_plan=${this._sellingPlan}`;
    }
    window.history.replaceState({ path: newurl }, '', newurl);
  }

  _updateAddToCartState(variant) {
    if (!variant && this._product.available) {
      for (let addToCart of this._addToCarts) {
        addToCart.setAttribute('disabled', 'disabled');
        let sub = addToCart.querySelector('[data-add-to-cart-text]') || addToCart;
        sub.innerHTML = this._ctx.getState().locale.makeASelection;
      }
      return;
    }

    if (!variant) {
      return;
    }

    let in_stock = variant.available;
    let variantStock = this._extraProductSettings.variants.find(
      oVariant => oVariant.id === variant.id
    );

    if (
      variantStock.inventory_policy === 'deny' &&
      variantStock.inventory_quantity < this._extraProductSettings.pack_size &&
      variantStock.inventory_management === 'shopify'
    ) {
      in_stock = false;
    }

    if (in_stock) {
      for (let [i, addToCart] of this._addToCarts.entries()) {
        addToCart.removeAttribute('disabled');
        let sub = addToCart.querySelector('[data-add-to-cart-text]') || addToCart;

        if (this._sellingPlan) {
          sub.innerHTML = this._ctx.getState().locale.subscribe;
        } else {
          sub.innerHTML = this._ctx.getState().locale.addToCart;
        }
      }
    } else {
      for (let addToCart of this._addToCarts) {
        addToCart.setAttribute('disabled', 'disabled');
        let sub = addToCart.querySelector('[data-add-to-cart-text]') || addToCart;
        sub.innerHTML = this._ctx.getState().locale.soldOut;
      }
    }
  }

  _switchToVariantImage(variant) {
    for (let productImage of this._images) {
      if (productImage.dataset.productImage) {
        let idArray = productImage.dataset.productImage.split(',');
        if (idArray.includes(variant.id.toString())) {
          let mainSlider = this._theme.getComponent('mainProductSlider');
          if (mainSlider) {
            mainSlider.component.goToSlide(productImage.dataset.index);
          }
        }
      }
    }
  }

  _updateProductPrices(variant) {
    let price = variant.price;
    let compare_at_price = variant.compare_at_price;

    if (this._sellingPlan) {
      let selling_plan_group = variant.selling_plan_allocations.find(
        allocation => `${allocation.selling_plan_id}` === `${this._sellingPlan}`
      );
      price = selling_plan_group.price;
      compare_at_price = selling_plan_group.compare_at_price;
    }

    price = price * this._extraProductSettings.pack_size;
    compare_at_price = compare_at_price * this._extraProductSettings.pack_size;

    for (let varies of this._priceVaries) {
      varies.classList.add('hide');
    }

    for (let productPrice of this._prices) {
      if (compare_at_price > price) {
        productPrice.classList.add('on-sale');
      } else {
        productPrice.classList.remove('on-sale');
      }
      productPrice.innerHTML = this._formatMoney(price);
    }

    for (let productComparePrice of this._comparePrices) {
      if (compare_at_price > price) {
        productComparePrice.classList.remove('hide');
        productComparePrice.innerHTML = this._formatMoney(compare_at_price);
      } else {
        productComparePrice.classList.add('hide');
        productComparePrice.innerHTML = '';
      }
    }

    if (!variant.available) {
      for (let productPrice of this._prices) {
        productPrice.innerHTML = this._ctx.getState().locale.unavailable;
      }
      for (let productComparePrice of this._comparePrices) {
        productComparePrice.classList.add('hide');
      }
    }

    for (let elem of this._perElems) {
      let toDivide = Number(elem.dataset.perPrice);
      elem.innerHTML = this._formatMoney(price / toDivide);
    }
  }

  _formatMoney(price) {
    return Helpers.formatMoney(price, this._ctx.getState().moneyFormat);
  }

  _updateMasterSelect(variant) {
    for (let originalSelector of this._masterSelects) {
      originalSelector.value = variant.id;
    }
  }

  _getCurrentOptions() {
    let currentOptions = [];
    for (let optionSelector of this._variantOptionTriggers) {
      let type = optionSelector.getAttribute('type');
      let currentOption = {};
      let alreadyDone = false;
      for (let option of currentOptions) {
        if (option.index === optionSelector.dataset.index) {
          alreadyDone = true;
        }
      }
      if (alreadyDone) continue;
      if (type === 'radio' || type === 'checkbox') {
        if (optionSelector.checked) {
          currentOption.value = optionSelector.value;
          currentOption.index = optionSelector.dataset.index;
          currentOptions.push(currentOption);
        } else {
          currentOptions.push(false);
        }
      } else {
        currentOption.value = optionSelector.value;
        currentOption.index = optionSelector.dataset.index;
        currentOptions.push(currentOption);
      }
    }
    currentOptions = Helpers.compact(currentOptions);
    return currentOptions;
  }

  _getVariantFromOptions() {
    let selectedValues = this._getCurrentOptions();
    let variants = this._product.variants;
    let found = false;

    if (selectedValues.length !== this._product.options.length) {
      return null;
    }

    for (let variant of variants) {
      var satisfied = true;

      for (let option of selectedValues) {
        if (satisfied) {
          satisfied = option.value === variant[option.index];
        }
      }

      if (satisfied) {
        found = variant;
      }
    }

    return found || null;
  }
}
