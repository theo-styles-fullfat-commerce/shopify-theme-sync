import Component from '../inherited/Component';
import Helpers from '../lib/Helpers';

class RangeSlider extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._rangeElem = this._elem.querySelector('[data-range-slider-elem]') || this._elem;

    this._minus = this._elem.querySelectorAll('[data-minus]');
    this._plus = this._elem.querySelectorAll('[data-plus]');
    this._options = {
      ...{
        behaviour: 'drag',
        minus: 0,
        animate: false
      },
      ...this._options
    };
    this._isMounted = false;

    console.log(this._options);
  }

  async mount() {
    const noUiSlider = await import('nouislider');
    noUiSlider.create(this._rangeElem, this._options);

    this._rangeElem.noUiSlider.on('slide', e => {
      if (this._options.callComponent) {
        let comp = this._theme.getComponent(this._options.callComponent).component;
        let value = Number(this._rangeElem.noUiSlider.get()) - this._options.minus;

        if (this._options.type === 'percent') {
          value = value / (this._options.range.max[0] - this._options.minus);
        }
        comp[this._options.callMethod](value);
      }
    });

    this._rangeElem.noUiSlider.on('end', e => {
      if (this._options.callComponent && this._options.callStopMethod) {
        let comp = this._theme.getComponent(this._options.callComponent).component;
        comp[this._options.callStopMethod]();
      }
    });

    for (let elem of this._minus) {
      elem.addEventListener('click', e => {
        let currentValue = Number(this._rangeElem.noUiSlider.get());
        this._rangeElem.noUiSlider.set(currentValue - 1);
      });
    }

    for (let elem of this._plus) {
      elem.addEventListener('click', e => {
        let currentValue = Number(this._rangeElem.noUiSlider.get());
        this._rangeElem.noUiSlider.set(currentValue + 1);
      });
    }

    this._ctx.emit('range-slider--ready');

    this._elem.dispatchEvent(new Event('range-slider--ready'));
    this._isMounted = true;
  }

  set(value) {
    if (this._rangeElem.noUiSlider) {
      this._rangeElem.noUiSlider.set(value);
    } else {
      this._elem.addEventListener('range-slider--ready', e => {
        this._rangeElem.noUiSlider.set(value);
      });
    }
  }
}

export default RangeSlider;
