import Component from '../inherited/Component';

export default class ClassChange extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
  }

  mount() {
    if (this._options.length) {
      for (let option of this._options) {
        this._optionLogic(option);
      }
    } else {
      this._optionLogic(this._options);
    }
  }

  _optionLogic(option) {
    if (!option.on) {
      option.on = 'click';
    }

    if (option.whenOutside) {
      document.addEventListener(
        option.on,
        e => {
          let clickIsInside = this._elem.contains(e.target) || this._elem === e.target;
          let toExclude = document.querySelectorAll(option.whenOutsideExcept);
          let hasClickedExclude = Array.from(toExclude).some(
            elem => elem.contains(e.target) || elem === e.target
          );

          if (clickIsInside || hasClickedExclude) return false;

          this._eventLogic(option, e);
        },
        option.preventDefault ? { passive: false } : { passive: true }
      );
    } else {
      this._elem.addEventListener(
        option.on,
        e => {
          this._eventLogic(option, e);
        },
        option.preventDefault ? { passive: false } : { passive: true }
      );
    }
  }

  _eventLogic(option, e) {
    if (option.onlyThisElem && e.target !== this._elem) {
      return false;
    }

    if (option.preventDefault) {
      e.preventDefault();
    }

    if (option.target.includes('closest:')) {
      let parentSelector = option.target.split('closest:')[1].trim();
      if (option.subTarget) {
        this._elem
          .closest(parentSelector)
          .querySelector(option.subTarget)
          .classList[option.method](option.class);
      } else {
        this._elem.closest(parentSelector).classList[option.method](option.class);
      }
    } else if (option.target === 'this') {
      this._elem.classList[option.method](option.class);
    } else {
      let targets = document.querySelectorAll(option.target);
      for (let target of targets) {
        target.classList[option.method](option.class);
      }
    }
  }
}
