import Component from '../inherited/Component'

export default class RecommendedProducts extends Component {
    constructor (elem, theme, options, ctx) {
      super(elem, theme, options, ctx)
      this._container = this._elem.querySelector('[data-recommended-products-container]')
    }

    async mount () {
      let axios = await import('axios')
      let url = `${this._options.baseUrl}?section_id=product-recommendations&product_id=${this._options.productId}&limit=${this._options.limit}`
      let { data } = await axios.get(url)

      let parser = new DOMParser()
      let html = parser.parseFromString(data, 'text/html')

      let items = html.querySelectorAll('[data-recommended-item]')
      let fragment = document.createDocumentFragment()

      if (items.length === 0) {
        this._elem.classList.add('hide')
      }

      for (let item of items) {
        fragment.appendChild(item)
      }

      this._theme.mountComponents(fragment)
      this._ctx.emit('ffc--lazy-load-update')

      this._container.append(fragment)

      if (this._options.compToMount) {
        let compObj = this._theme.getComponent(this._options.compToMount)
        if (compObj) {
          compObj.component._options.mount = true
          compObj.component.mount()
        }
      }
    }
}