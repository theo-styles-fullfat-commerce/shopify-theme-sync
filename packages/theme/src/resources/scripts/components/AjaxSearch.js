import Component from '../inherited/Component';
import Helpers from '../lib/Helpers';

let axios = null;

export default class AjaxSearch extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._response = null;
    this._form = this._elem.querySelector('[data-ajax-search-form]');
    this._triggers = this._elem.querySelectorAll('[data-ajax-search-trigger]');
    this._input = this._elem.querySelector('[data-ajax-search-input]');
    this._results = this._elem.querySelector('[data-ajax-search-results]');
    this._clearTrigger = null;
    this._exitTrigger = this._elem.querySelector('[data-ajax-search-exit]');
    this._loading = false;
  }

  async mount() {
    axios = await import('axios');

    console.log(axios);

    console.log(axios.get());

    this._form.addEventListener('submit', e => {
      e.preventDefault();
      this._submitForm();
    });

    this._input.addEventListener('input', () => {
      if (this._input.value.length === 0) {
        this._clearSearch();
      }
    });

    this._input.addEventListener(
      'input',
      Helpers.debounce(() => {
        if (this._input.value.length !== 0) {
          this._submitForm();
        }
      }, 300)
    );

    this._exitTrigger.addEventListener('click', e => {
      e.preventDefault();
      this._clearSearch();
    });

    if (this._input.value) {
      this._elem.classList.add('open');
      this._submitForm();
    }

    for (let trigger of this._triggers) {
      trigger.addEventListener('click', e => {
        this._elem.scrollTo({
          top: 0,
          behavior: 'smooth'
        });

        this._input.value = trigger.dataset.ajaxSearchTrigger;
        this._submitForm();
      });
    }
  }

  async _submitForm() {
    if (this._loading) return false;

    let valid = this._form.reportValidity();

    if (valid) {
      this._loading = true;
      this._elem.classList.add('ajax-search--loading');

      let url = `/search?view=ajax&q=${this._input.value}&type=product`;

      if (this._input.value.split(' ').length === 1) {
        url += `&options[prefix]=last`;
      }

      let { data } = await axios.get(url);
      let parser = new DOMParser();
      let html = parser.parseFromString(data, 'text/html');
      let fetchedElem = html.querySelector('[data-ajax-search-results]');
      this._results.innerHTML = fetchedElem.innerHTML;

      this._clearTrigger = this._elem.querySelector('[data-ajax-search-clear]');

      this._clearTrigger.addEventListener('click', e => {
        e.preventDefault();
        e.stopPropagation();
        this._clearSearch();
      });

      this._theme.mountComponents(this._results);

      this._ctx.emit('ffc--scroll-lock-elem', null, this._elem);
      this._ctx.emit('ffc--lazy-load-update');
      this._ctx.emit('ffc--quicklink-listen-to', null, this._results);

      this._elem.classList.remove('ajax-search--loading');
      this._loading = false;
    }
  }

  _clearSearch() {
    this._results.innerHTML = '';
    this._input.value = '';
  }
}
