import Component from '../inherited/Component';

export default class StickyATC extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._lastScrollY = window.scrollY;
    this._currentScrollY = window.scrollY;
    this._currentDirection = 'down';
  }

  mount() {
    this._assignVariables();
    window.addEventListener(
      'scroll',
      e => {
        this._assignVariables();
        this._doScrollCalculations();
      },
      { passive: true }
    );
  }

  _doScrollCalculations() {
    if (this._currentDirection === 'down') {
      this._elem.classList.remove('shown');
    }

    if (this._currentDirection === 'up') {
      this._elem.classList.add('shown');
    }
  }

  _assignVariables() {
    this._currentScrollY = window.scrollY;
    this._currentDirection = this._currentScrollY > this._lastScrollY ? 'down' : 'up';
    this._lastScrollY = window.scrollY;
  }
}
