import Component from '../inherited/Component';
import Helpers from '../lib/Helpers';

export default class CopyLink extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
  }

  mount() {
    this._elem.addEventListener('click', e => {
      e.preventDefault();
      Helpers.copyToClipboard(this._options.link);

      let elem = this._options.elem ? document.querySelector(this._options.elem) : this._elem;
      elem.textContent = 'Copied!';
      elem.classList.add('shown');
    });
  }
}
