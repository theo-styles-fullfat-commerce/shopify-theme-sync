import Component from '../inherited/Component';
import Helpers from '../lib/Helpers';
import AjaxApi from '../lib/AjaxApi';

class Cart extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._data = {
      cart: {}
    };
    this._renderAreas = document.querySelectorAll('[data-cart-mount]');
    this._jquery = window.$ || null;
    this._extraSettings = document.querySelector('[data-extra-cart-json]')
      ? JSON.parse(document.querySelector('[data-extra-cart-json]').innerHTML)
      : {};
    this._rivets = null;
    this._OOSboxes = [];
  }

  _binders() {
    this._rivets.formatters.getSizedImage = (src, size) => {
      return src ? Helpers.getSizedImageUrl(src, size) : '';
    };

    this._rivets.formatters.money = value => {
      return Helpers.formatMoney(value, this._ctx.getState().moneyFormat);
    };

    this._rivets.formatters.length = arr => {
      return arr ? arr.length : 0;
    };

    this._rivets.formatters.eq = (arg1, arg2) => {
      return arg1 === arg2;
    };

    this._rivets.formatters.gt = (arg1, arg2) => {
      return arg1 > arg2;
    };

    this._rivets.formatters.append = (str1, str2) => {
      return `${str1}${str2}`;
    };

    this._rivets.formatters.atPackThreshold = (quantity, item) => {
      if (item.isBox) return false;
      let pack_size = item.product.metafields.pack_size
        ? Number(item.product.metafields.pack_size)
        : 0;
      return quantity <= pack_size;
    };

    this._rivets.formatters.pluralize = (count, one, multiple) => {
      return count === 1 ? one : multiple;
    };

    this._rivets.formatters.getProps = item => {
      let props = [];
      for (let key in item.properties) {
        if (key.charAt(0) === '_') continue;
        let name = key.charAt(0).toUpperCase() + key.slice(1);
        props.push({ name: name, value: item.properties[key] });
      }
      return props;
    };
  }

  async mount() {
    this._rivets = await import('rivets');

    this._data.cart = await this.prepareData(JSON.parse(this._elem.innerHTML));

    this._ctx.on('cart-item-added', async () => {
      this._data.cart.loading = true;
      await this._refresh();
      for (let area of this._renderAreas) {
        area.classList.add('shown');
      }
    });

    document.addEventListener('ffc--refresh-cart', async e => {
      this._data.cart.loading = true;
      await this._refresh();
      for (let area of this._renderAreas) {
        area.classList.add('shown');
      }
    });

    this._removeOOSboxes();
    this._binders();
    this._render();

    if (window.Shopify && Shopify.StorefrontExpressButtons) {
      Shopify.StorefrontExpressButtons.initialize();
    }
  }

  async _refresh() {
    this._data.cart = await this.prepareData(await AjaxApi.getCart());

    if (window.Shopify && Shopify.StorefrontExpressButtons) {
      Shopify.StorefrontExpressButtons.initialize();
    }
  }

  async _removeOOSboxes() {
    for (let boxId of this._OOSboxes) {
      for (let item of this._data.cart.items) {
        if (item.properties['_box-id'] === boxId) {
          await AjaxApi.removeCartItemById(item.variant_id);
        }
      }
    }

    await this._refresh();
  }

  async prepareData(data) {
    data.displayItems = [];

    let promises;
    let errored;

    const getItems = async (iteration = 0) => {
      if (iteration > 10) return false;
      iteration++;

      promises = [];
      errored = false;

      for (const [i, item] of data.items.entries()) {
        promises.push(
          new Promise(async (res, rej) => {
            item.index = i + 1;

            try {
              let prodData = await AjaxApi.getCustomProduct(item.handle);
              item.product = prodData.product;
              item.product.metafields = prodData.metafields;
              item.image = Helpers.getSizedImageUrl(item.image, '200x');
              item.variantStock = prodData.variantStock;

              let variant = item.variant;
              let variantQuantity = item.variantStock.find(
                variant => variant.id === item.variant_id
              );

              // if (item.title === 'COLOMBIAN COFFEE, BANANA & CINNAMON') { // for testing purposes only
              if (
                variantQuantity.inventory_policy === 'deny' &&
                variantQuantity.inventory_quantity < 1
              ) {
                if (item.properties && item.properties['_box-id']) {
                  this._OOSboxes.push(item.properties['_box-id']);
                }
              } else {
                let packSize = item.product.metafields.pack_size
                  ? Number(item.product.metafields.pack_size)
                  : 1;
                item.displayQuantity = item.quantity / packSize;
                item.singlePrice = item.price * packSize;

                if (!this._isPartOfBox(item)) {
                  data.displayItems.push(item);
                }
              }

              return res();
            } catch (err) {
              await AjaxApi.removeCartItemById(item.variant_id);
              errored = true;
              return res();
            }
          })
        );
      }

      console.log(data.items);

      await Promise.all(promises);

      if (errored) {
        data.items = await AjaxApi.getCart();
        await getItems();
      }
    };

    await getItems();

    let boxes = data.items
      .filter((item, index, self) => {
        let otherBox =
          index ===
          self.findIndex(
            t =>
              item.properties &&
              t.properties &&
              item.properties['_box-id'] === t.properties['_box-id']
          );
        return this._isPartOfBox(item) && otherBox;
      })
      .map(item => {
        let itemsInBox = data.items.filter(
          t => t.properties && t.properties['_box-id'] === item.properties['_box-id']
        );

        let totalPrice = itemsInBox.reduce((acc, value) => {
          return acc + value.line_price;
        }, 0);

        let totalQuantity = itemsInBox.reduce((acc, value) => {
          return acc + value.quantity;
        }, 0);

        let singlePrice = itemsInBox.reduce((acc, value) => {
          let step = Number(value.properties['_box-base-quantity']) || 1;
          return acc + value.price * step;
        }, 0);

        let boxSize = Number(item.properties['_box-size']);
        let boxQuantity = totalQuantity / boxSize;

        let image = this._extraSettings.build_a_box_image || item.image;
        console.log(image);
        return {
          image: image,
          product_title: 'Build a Box',
          variant_title: `${totalQuantity} items`,
          singlePrice: singlePrice,
          line_price: totalPrice,
          quantity: boxQuantity,
          displayQuantity: Math.round(boxQuantity),
          subItems: itemsInBox,
          properties: item.properties,
          isBox: true,
          selling_plan_allocation: item.selling_plan_allocation
        };
      });

    // Put boxes first in the cart
    data.displayItems = boxes.concat(data.displayItems);

    data.displayCount = data.displayItems.reduce((acc, value) => {
      return value.displayQuantity + acc;
    }, 0);
    data.displayCount = Math.round(data.displayCount);

    data.loading = false;

    console.log(data);

    return data;
  }

  _render() {
    for (let cartSection of this._renderAreas) {
      this._rivets.bind(cartSection, this._getModel(cartSection));
    }

    this._data.cart.loading = false;

    for (let cartSection of this._renderAreas) {
      cartSection.classList.add('rendered');
    }
  }

  _getModel(cartSection) {
    return {
      data: this._data,
      controller: {
        removeItem: async (e, model) => {
          e.preventDefault();
          model.data.cart.loading = true;
          let item = model.item;

          if (item.isBox) {
            await this._updateBoxQuantity(item, 0, false);
          } else {
            await AjaxApi.removeCartItem(item.index);
            await this._refresh();
          }
        },
        updateQuantity: async (e, model) => {
          e.preventDefault();
          let item = model.item;

          if (item.isBox) {
            await this._updateBoxQuantity(item, item.quantity);
          } else {
            await this._updateQuantity(model);
          }
        },
        incrementQuantity: async (e, model) => {
          e.preventDefault();
          let item = model.item;
          item.displayQuantity++;

          if (item.isBox) {
            await this._updateBoxQuantity(item, 'up');
          } else {
            await model.controller.updateQuantity(e, model);
          }
        },
        decrementQuantity: async (e, model) => {
          e.preventDefault();
          let item = model.item;
          item.displayQuantity--;

          if (item.isBox) {
            await this._updateBoxQuantity(item, 'down');
          } else {
            await model.controller.updateQuantity(e, model);
          }
        },
        clearError: (e, model) => {
          e.preventDefault();
          model.data.errorMessage = null;
        }
      }
    };
  }

  async _updateBoxQuantity(item, direction, isStep = true) {
    let updates = this._getUpdatesMap();

    for (let subItem of item.subItems) {
      let step = Number(subItem.properties['_box-base-quantity']) || 1;
      step *= direction === 'up' ? 1 : -1;
      updates[subItem.index - 1] = isStep ? updates[subItem.index - 1] + step : 0;
    }

    try {
      this._data.cart.loading = true;
      await AjaxApi.updateCart({
        updates: updates
      });
      await this._refresh();
    } catch (err) {
      this._data.cart.loading = false;
      this._data.errorMessage = `Cannot update box quantity, insufficient stock`;
    }
  }

  async _updateQuantity(model) {
    let item = model.item;
    if (item.displayQuantity.length && isNaN(parseInt(item.displayQuantity))) {
      item.displayQuantity = 1;
      return false;
    }

    let packSize = item.product.metafields.pack_size
      ? Number(item.product.metafields.pack_size)
      : 1;
    let newQuantity = item.displayQuantity * packSize;

    let variantQuantity = item.variantStock.find(variant => variant.id === item.variant_id);

    if (
      variantQuantity.inventory_policy === 'deny' &&
      variantQuantity.inventory_quantity < newQuantity &&
      variantQuantity.inventory_management === 'shopify'
    ) {
      this._data.errorMessage = `All ${item.title} are currently in your cart`;
      await this._refresh();
      return false;
    }

    model.data.cart.loading = true;

    try {
      await AjaxApi.updateCartItem(item.index, newQuantity, {});
      await this._refresh();
    } catch (err) {
      model.data.cart.loading = false;
      this._data.errorMessage = `All ${item.title} are currently in your cart`;
    }
  }

  _isPartOfBox(item) {
    return item.properties && item.properties['_box-id'];
  }

  _getUpdatesMap(items = this._data.cart.items) {
    return items.map(item => item.quantity);
  }
}

export default Cart;
