import AjaxApi from '../lib/AjaxApi';
import Component from '../inherited/Component';

export default class SetTimeToday extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
  }

  async mount() {
    this._addTimeToday();
  }

  async _addTimeToday() {
    let timeToday = Math.floor(Date.now() / 1000);

    await AjaxApi.updateCart({
      attributes: {
        _time_today: timeToday
      }
    });
  }
}
