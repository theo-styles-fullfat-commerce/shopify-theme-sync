import Component from '../inherited/Component'
import Helpers from '../lib/Helpers'

export default class LockScroll extends Component {
  constructor (elem, theme, options, ctx) {
    super(elem, theme, options, ctx)
  }

  async mount () {
    let { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } = await import('body-scroll-lock')

    this._disableBodyScroll = disableBodyScroll
    this._enableBodyScroll = enableBodyScroll
    this._clearAllBodyScrollLocks = clearAllBodyScrollLocks

    if (this._options.length) {
      for (let option of this._options) {
        this._optionLogic(option)
      }
    } else {
      this._optionLogic(this._options)
    }

    this._ctx.on('ffc--scroll-lock-elem', (state, data) => {
      this._lockScroll(data)
    })

    this._ctx.on('ffc--clear-all-scroll-locks', (state, data) => {
      this._clearAllBodyScrollLocks()

      let lockedElems = document.querySelectorAll('[data-is-locked="true"]')
      for (let elem of lockedElems) {
        elem.removeAttribute('data-is-locked')
      }
    })
  }

  _optionLogic(option) {
    if (!option.on) {
      option.on = 'click'
    }

    if (option.whenOutside) {
      document.addEventListener(option.on, e => {
        let clickIsInside = this._elem.contains(e.target) || this._elem === e.target
        let toExclude = document.querySelectorAll(option.whenOutsideExcept)
        let hasClickedExclude = Array.from(toExclude).some(elem => elem.contains(e.target) || elem === e.target)

        let target = document.querySelector(option.target)

        let hasClickedOtherLockScroll = Helpers.hasParentWithSelector(e.target, '[data-lock-scroll]')

        if (hasClickedOtherLockScroll && !clickIsInside && !hasClickedExclude) {
          target.removeAttribute('data-is-locked')
        }

        if (!target.hasAttribute('data-is-locked')) {
          return false
        }

        if (clickIsInside || hasClickedExclude) return false

        this._eventLogic(option, e)
      
      }, option.preventDefault ? { passive: false } : { passive: true })
    } else {
      this._elem.addEventListener(option.on, e => {
        this._eventLogic(option, e)
      }, option.preventDefault ? { passive: false } : { passive: true })
    }
  }

  _eventLogic(option, e) {
    if (option.onlyThisElem && e.target !== this._elem) {
      return false
    }

    if (option.preventDefault) {
      e.preventDefault()
    }

    this._clearAllBodyScrollLocks()

    let target = document.querySelector(option.target)

    if (option.method === 'lockScroll') {
      this._lockScroll(target)
    } else if (option.method === 'toggleScroll') {
      if (!target.hasAttribute('data-is-locked')) {
        this._disableBodyScroll(target, { reserveScrollBarGap: true })
        target.setAttribute('data-is-locked', true)
      } else {
        this._clearAllBodyScrollLocks()
        target.removeAttribute('data-is-locked')
      }
    } else {
      let lockedElems = document.querySelectorAll('[data-is-locked="true"]')
      for (let elem of lockedElems) {
        elem.removeAttribute('data-is-locked')
      }
    }
  }

  _lockScroll(elem) {
    this._clearAllBodyScrollLocks()
    let lockedElems = document.querySelectorAll('[data-is-locked="true"]')
    for (let elem of lockedElems) {
      elem.removeAttribute('data-is-locked')
    }
    
    this._disableBodyScroll(elem, { reserveScrollBarGap: true })
    elem.setAttribute('data-is-locked', true)
  }
}