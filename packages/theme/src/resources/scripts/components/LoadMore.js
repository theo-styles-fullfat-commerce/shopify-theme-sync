import Component from '../inherited/Component'

let axios = null

export default class LoadMore extends Component {
  constructor (elem, theme, options, ctx) {
    super(elem, theme, options, ctx)
    this._loadMoreContainer = this._elem.querySelector('[data-load-more-container]')

    this._loadMoreTriggerContainer = this._elem.querySelector('[data-load-more-trigger-container]')
    this._loadMoreTrigger = this._loadMoreTriggerContainer ? this._loadMoreTriggerContainer.querySelector('[data-load-more-trigger]') : null

    this._loadPreviousTriggerContainer = this._elem.querySelector('[data-load-previous-trigger-container]')
    this._loadPreviousTrigger = this._loadPreviousTriggerContainer ? this._loadPreviousTriggerContainer.querySelector('[data-load-previous-trigger]') : null


    this._loading = false
    this._originalText = this._loadMoreTrigger ? this._loadMoreTrigger.innerHTML : ''
    this._pageNumber = this._options.basePage
    this._currentPage = this._options.basePage

    this._baseURL = `${window.location.protocol}//${window.location.host}${window.location.pathname}`
  }

  async mount () {
    axios = await import('axios')
    if (this._loadMoreTrigger) {
      this._loadMoreTrigger.addEventListener('click', e => {
        e.preventDefault();
        this._load(this._loadMoreTrigger, this._loadMoreTriggerContainer, 'next')
      })
    }

    if (this._loadPreviousTrigger) {
      this._loadPreviousTrigger.addEventListener('click', e => {
        e.preventDefault();
        this._load(this._loadPreviousTrigger, this._loadPreviousTriggerContainer, 'previous')
      })
    }

    if (this._options.historyState) {
      requestAnimationFrame(this._trackScroll.bind(this))
    }

    if (this._options.infiniteScroll) {
      this._setupInfiniteScroll()
    }
  }

  async _load (trigger, container, type) {
    if (this._loading) return false
    this._loading = true

    container.classList.add('ffc--loading')

    trigger.innerHTML = this._options.loadingText

    let { data } = await axios.get(trigger.getAttribute('href'))

    if (type == 'next') {
      this._pageNumber++
    } else {
      this._pageNumber--
    }

    let parser = new DOMParser()
    let html = parser.parseFromString(data, 'text/html')

    let items = html.querySelectorAll('[data-load-more-container] > *')
    let fragment = document.createDocumentFragment()

    for (let item of items) {
        fragment.appendChild(item)
        item.setAttribute('data-page-number', this._pageNumber)
    }

    this._theme.mountComponents(fragment)

    let newLoadMoreTrigger = null
    if (type === 'next') {
      this._loadMoreContainer.appendChild(fragment)
      newLoadMoreTrigger = html.querySelector('[data-load-more-trigger]')
    } else {
      this._loadMoreContainer.prepend(fragment)
      newLoadMoreTrigger = html.querySelector('[data-load-previous-trigger]')
    }

    if (newLoadMoreTrigger) {
      trigger.setAttribute('href', newLoadMoreTrigger.getAttribute('href'))
    } else {
      container.parentNode.removeChild(container)
    }
    this._loading = false

    container.classList.remove('ffc--loading')

    trigger.innerHTML = this._originalText

    this._ctx.emit('ffc--lazy-load-update')
    this._ctx.emit('ffc--quicklink-listen-to', null, this._elem)
  }

  _setupInfiniteScroll () {
    let observer = new IntersectionObserver((entries, observer) => {
      for (let entry of entries) {
        if (entry.isIntersecting) {
          if (this._loadMoreTriggerContainer && entry.target === this._loadMoreTriggerContainer) {
            console.log("LOAD MORE")
            this._load(this._loadMoreTrigger, this._loadMoreTriggerContainer, 'next')
          } else if ( this._loadPreviousTriggerContainer && entry.target === this._loadPreviousTriggerContainer) {
            console.log("LOAD PREV")
            this._load(this._loadPreviousTrigger, this._loadPreviousTriggerContainer, 'prev')
          }
        }
      }
    }, {
      threshold: 0,
      rootMargin: "500px"
    })

    if (this._loadMoreTriggerContainer) {
      observer.observe(this._loadMoreTriggerContainer)
    }

    if (this._loadPreviousTriggerContainer) {
      observer.observe(this._loadPreviousTriggerContainer)
    }
  }

  _trackScroll () {
    let firstElemInView = null
    for (let elem of this._loadMoreContainer.children) {
      if (elem.getBoundingClientRect().top >= 0) {
        firstElemInView = elem
        break
      }
    }

    if(firstElemInView) {
      let pageNumber = firstElemInView.getAttribute('data-page-number')
      if (pageNumber && Number(pageNumber) !== this._currentPage) {
        this._currentPage = Number(pageNumber)
        this._updateHistoryState()
      } else if (!pageNumber && this._currentPage !== this._options.basePage) {
        this._currentPage = this._options.basePage
        this._updateHistoryState()
      }
    }

    requestAnimationFrame(this._trackScroll.bind(this))
  }

  _updateHistoryState () {
    let newurl = `${this._baseURL}?page=${this._currentPage}`
    window.history.replaceState({ path: newurl }, '', newurl)
  }
}