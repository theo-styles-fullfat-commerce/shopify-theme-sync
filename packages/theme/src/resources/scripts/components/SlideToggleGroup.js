import Component from '../inherited/Component';
import Helpers from '../lib/Helpers';

export default class SlideToggleGroup extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._toggleItems = this._elem.querySelectorAll('[data-toggle-item]');
  }

  mount() {
    if (
      this._options.closeOnOutsideClick &&
      matchMedia('only screen and (min-width: 835px)').matches
    ) {
      document.addEventListener(
        'click',
        e => {
          let clickIsInside = this._elem.contains(e.target) || this._elem === e.target;

          if (clickIsInside) return false;

          for (let item of this._toggleItems) {
            let elemToSlide = item.querySelector('[data-toggle-elem]');
            if (item.classList.contains('open')) {
              Helpers.slideUp(elemToSlide, item);
            }
          }
        },
        { passive: true }
      );
    }

    for (let item of this._toggleItems) {
      let trigger = item.querySelector('[data-toggle-trigger]');

      trigger.addEventListener('click', e => {
        e.preventDefault();

        let elemToSlide = item.querySelector('[data-toggle-elem]');

        if (elemToSlide.dataset.transitioning === 'true') {
          return false;
        }

        if (item.classList.contains('open')) {
          Helpers.slideUp(elemToSlide, item);
        } else {
          Helpers.slideDown(elemToSlide, item);
        }

        let otherToggles = item.parentNode.querySelectorAll('[data-toggle-item]');

        for (let otherItem of otherToggles) {
          if (
            otherItem !== item &&
            otherItem.classList.contains('open') &&
            otherItem.dataset.transitioning !== 'true'
          ) {
            let elemToSlide = otherItem.querySelector('[data-toggle-elem]');
            Helpers.slideUp(elemToSlide, otherItem);
          }
        }
      });
    }
  }
}
