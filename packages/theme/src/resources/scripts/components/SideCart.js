import Component from '../inherited/Component'

class SideCart extends Component {
    constructor (elem, theme, options, ctx) {
        super(elem, theme, options, ctx)
        this._scrollArea = this._elem.querySelector('[data-side-cart-scroll]')
        this._cartTrigger = document.querySelector('[data-cart-trigger]')
    }

    async mount () {
        this._ctx.on('cart-item-added', async () => {
            this.showSideCart()
        })
        let params = new URLSearchParams(window.location.search)
        if (params.has('show-cart')) {
            this.showSideCart()
        }
    }

    showSideCart () {
        this._scrollArea = this._elem.querySelector('[data-side-cart-scroll]')
        this._elem.classList.add('shown')
        this._cartTrigger.classList.add('active')
        this._ctx.emit('ffc--scroll-lock-elem', null, this._scrollArea)
    }
}

export default SideCart
