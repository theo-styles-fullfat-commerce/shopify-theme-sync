import Component from '../inherited/Component'

export default class SelectURL extends Component {
  constructor (elem, theme, options, ctx) {
    super(elem, theme, options, ctx)
  }

  mount () {
    this._elem.addEventListener('change', e => {
      window.location = this._elem.value
    })
  }
}