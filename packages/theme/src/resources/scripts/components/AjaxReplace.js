import Component from '../inherited/Component'

let axios = null

export default class AjaxReplace extends Component {
    constructor (elem, theme, options, ctx) {
      super(elem, theme, options, ctx)
      this._response = null
    }

    async mount () {
      axios = await import('axios')

      
      if (this._options.preloadOn) {
        this._elem.addEventListener(this._options.preloadOn, async e => {
          let ajaxCache = this._ctx.getState().ajaxCache
          let cachedResponse = ajaxCache.find(resp => resp.url === this._options.url)
          if (cachedResponse) {
            this._response = cachedResponse.response
          } else {
            this._response = axios.get(this._options.url)
            await this._response
            let newCache = ajaxCache.slice(0)
            newCache.push({
              url: this._options.url,
              response: this._response
            })
            this._ctx.hydrate({
              ajaxCache: newCache
            })
          }
        })
      }
      this._elem.addEventListener(this._options.on, async e => {
        e.preventDefault()
        let elemToReplace = null

        if (this._options.replace.includes('closest:')) {
          let parentSelector = this._options.replace.split('closest:')[1].trim()
          elemToReplace = this._elem.closest(parentSelector)
        } else {
          elemToReplace = this._options.replace === 'this' ? this._elem : document.querySelector(this._options.replace)
        }

        if (elemToReplace) {
          elemToReplace.classList.add('loading')
        }

        let toWaitFor = null
        if (this._options.preloadOn) {
          toWaitFor = this._response
        } else {
          toWaitFor = axios.get(this._options.url)
        }
        let resp = await toWaitFor
        let parser = new DOMParser()
        let html = parser.parseFromString(resp.data, 'text/html')
        let fetchedElem = html.querySelector(this._options.fetch)
        fetchedElem.classList.add('loading')

        if (elemToReplace) {
            elemToReplace.parentNode.replaceChild(fetchedElem, elemToReplace)
            this._theme.mountComponents(fetchedElem)
            this._ctx.emit('ffc--lazy-load-update')
            fetchedElem.classList.remove('loading')

            if (this._options.updateHistoryState) {
              this._updateHistoryState(this._options.url)
            }

            if (this._options.scrollToTopWhen && matchMedia(`only screen and ${this._options.scrollToTopWhen}`).matches) {
              window.scrollTo({
                top: 0,
                behavior: 'smooth'
              })
            }
        }
      })
    }

    _updateHistoryState (url) {
      let newurl = `${window.location.protocol}//${window.location.host}${url}`
      window.history.replaceState({ path: newurl }, '', newurl)
    }
}