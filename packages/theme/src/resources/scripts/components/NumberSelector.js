import Component from '../inherited/Component';

export default class NumberSelector extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._input = this._elem.querySelector('[data-input]');
    this._minus = this._elem.querySelector('[data-minus]');
    this._plus = this._elem.querySelector('[data-plus]');
    this._max = this._input.getAttribute('max') ? Number(this._input.getAttribute('max')) : null;
    this._min = this._input.getAttribute('min') ? Number(this._input.getAttribute('min')) : null;
  }

  mount() {
    this._input.addEventListener('input', e => {
      let currentValue = Number(this._input.value);
      if (this._min && currentValue < this._min) {
        this._input.value = this._min;
      }
      if (this._max && currentValue > this._max) {
        this._input.value = this._max;
      }
    });

    this._minus.addEventListener('click', e => {
      e.preventDefault();
      let newValue = Number(this._input.value) - 1;
      if (this._min && newValue < this._min) {
        return false;
      }
      this._input.value = newValue;
      this._input.dispatchEvent(new Event('change'));
    });

    this._plus.addEventListener('click', e => {
      e.preventDefault();
      let newValue = Number(this._input.value) + 1;
      if (this._max && newValue > this._max) {
        return false;
      }
      this._input.value = newValue;
      this._input.dispatchEvent(new Event('change'));
    });
  }
}
