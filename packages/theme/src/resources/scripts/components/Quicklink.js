import Component from '../inherited/Component';

export default class Quicklink extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._options = {
      ...{
        limit: 50,
        ignores: [
          url => {
            let toTest = url.replace(document.location.origin, '');
            let toIgnore = [
              '/account/logout',
              '/account/login',
              '/cart/clear',
              '/cart/add',
              '/cart/update',
              '/cart/change',
              '/pages/privacy-policy',
              '/pages/policies',
              '/#swym-wishlist',
              '/search',
              '/#',
              '/#main',
              '/checkout'
            ];
            let ignoring = toIgnore.some(urlToIgnore => toTest.includes(urlToIgnore));

            return ignoring;
          }
        ]
      },
      ...this._options
    };
  }

  async mount() {
    let quicklink = await import('quicklink');

    window.addEventListener('load', async e => {
      this._options.el = this._elem;
      quicklink.listen(this._options);
    });

    this._ctx.on('ffc--quicklink-listen-to', (state, elem) => {
      console.log(elem);
      this._options.el = elem;
      quicklink.listen(this._options);
    });
  }
}
