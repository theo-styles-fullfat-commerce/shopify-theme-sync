import Component from '../inherited/Component';
import { create } from 'evx';
import Helpers from '../lib/Helpers';
import KeenSlider from 'keen-slider';

export default class KeenSliderComp extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._options = {
      ...{
        slidesPerView: 1,
        dragSpeed: 1,
        autoplaySpeed: 5000,
        duration: 500
      },
      ...this._options
    };
    this._slider = null;
    this._autoplayInterval = null;
    this.shouldAutoplay = false;
    this._localCtx = create();
    this._isMounted = false;

    this._lastRangeAmount = 0;
    this._scrollWidth = this._elem.scrollWidth;

    super.reinitOnResize();
  }

  async mount() {
    if (
      this._options.enableWhen &&
      !matchMedia(`only screen and ${this._options.enableWhen}`).matches
    ) {
      window.addEventListener(
        'resize',
        e => {
          if (
            matchMedia(`only screen and ${this._options.enableWhen}`).matches &&
            !this._isMounted
          ) {
            this.mount();
          } else if (
            !matchMedia(`only screen and ${this._options.enableWhen}`).matches &&
            this._isMounted
          ) {
            this.unmount();
            this._isMounted = false;
          }
        },
        { passive: true }
      );
      return false;
    }
    if (this._options.mount === false) {
      return false;
    }

    this._elem.classList.add('keen-slider');
    for (let elem of this._elem.children) {
      if (elem.hasAttribute('data-keen-dots') || elem.hasAttribute('data-keen-nav')) continue;
      elem.classList.add('keen-slider__slide');
      if (this._options.fade) {
        elem.classList.add('fader-slide');
      }
    }
    this._slides = this._elem.querySelectorAll('.keen-slider__slide');

    // AS NAV FOR
    if (this._options.asNavFor) {
      this._parentSlider = this._theme.getComponent(this._options.asNavFor);
      this._parentSlider = this._parentSlider.component;

      this._parentSlider._localCtx.on('slide-change', (state, data) => {
        this.goToSlide(data.slide);
      });
    }

    if (this._options.arrows) this._createArrows();
    if (this._options.dots && this._slides.length > 1) this._createDots();

    if (this._slides.length === this._options.slidesPerView) {
      this._options.loop = false;
      this._options.controls = false;
    }

    this._dotsContainer = this._options.dotsContainer
      ? document.querySelector(this._options.dotsContainer)
      : this._elem;
    this._dots = this._dotsContainer ? this._dotsContainer.querySelectorAll('[data-dot]') : [];

    if (!this._prevArrow) this._prevArrow = this._elem.querySelector('[data-keen-prev]');
    if (!this._nextArrow) this._nextArrow = this._elem.querySelector('[data-keen-next]');

    this._options.slideChanged = slider => {
      this.updateClasses(slider);
      this.updateFadeHeight(slider);
      this._localCtx.emit('slide-change', null, {
        slide: slider.details().relativeSlide
      });
      this.adaptiveHeight(slider);
      if (this._isMounted) this.scrollRevealFix(slider);

      this._doSlideCSSVars(slider);
      this._moveRangeSlider(slider);

      this.autoplay();
    };

    this._options.created = slider => {
      this._elem.classList.add('keen-slider--ready');
      this.updateClasses(slider);
      this.updateFadeHeight(slider);
      this.adaptiveHeight(slider);
      this._doSlideCSSVars(slider);
      this._scrollWidth = this._elem.scrollWidth;

      if (this._options.alignArrowsTo) this.alignArrows();

      window.addEventListener(
        'resize',
        e => {
          this.updateFadeHeight();
          this.adaptiveHeight();

          if (this._options.alignArrowsTo) {
            Helpers.nextFrame(() => {
              this.alignArrows();
            });
          }
        },
        { passive: true }
      );
    };

    this._options.dragStart = () => {
      this.shouldAutoplay = false;
    };

    this._options.dragEnd = () => {
      this.shouldAutoplay = true;
    };

    if (this._options.fade) {
      this._options.move = slider => this.fader(slider);
    }

    this._slider = new KeenSlider(this._elem, this._options);

    if (this._prevArrow) this._prevArrow.addEventListener('click', e => this.goToPrev(e));
    if (this._nextArrow) this._nextArrow.addEventListener('click', e => this.goToNext(e));

    for (let dot of this._dots) {
      dot.addEventListener('click', e => {
        e.preventDefault();
        this.goToSlide(dot.dataset.dot);

        if (this._parentSlider) {
          this._parentSlider.goToSlide(dot.dataset.dot);
        }
      });
    }

    if (this._options.autoplay) {
      this._setupAutoplay();
    }

    this._isMounted = true;

    if (this._options.triggerOnMount) {
      this._ctx.emit(this._options.triggerOnMount);
    }
  }

  unmount() {
    this._slider.destroy();
    if (this._navContainer) {
      this._navContainer.parentNode.removeChild(this._navContainer);
    }
    this._elem.classList.remove('keen-slider');
    for (let elem of this._elem.children) {
      elem.classList.remove('keen-slider__slide');
      elem.classList.remove('fader-slide');
      elem.removeAttribute('style');
    }
  }

  _moveRangeSlider(slider) {
    if (!this._options.rangeSlider) return false;
    let slideIndex = slider.details().relativeSlide;
    let rangeComponent = this._theme.getComponent(this._options.rangeSlider);
    rangeComponent.component.set(slideIndex + 1);
  }

  _setupAutoplay() {
    this.shouldAutoplay = true;
    this.autoplay();

    if (this._options.pauseAutoplayOnHover) {
      this._elem.addEventListener('mouseover', e => {
        this.shouldAutoplay = false;
      });

      this._elem.addEventListener('mouseout', e => {
        this.shouldAutoplay = true;
      });
    }
  }

  _doSlideCSSVars(slider = this._slider) {
    let slide = this.getActiveSlideElem(slider);

    if (slide && slide.dataset.slideCssVars) {
      let varArr = JSON.parse(slide.dataset.slideCssVars);

      for (let cssVar of varArr) {
        let toSetOn = document.querySelector(cssVar.onElem);

        if (toSetOn) {
          toSetOn.style.setProperty(cssVar.var, cssVar.value);
        }
      }
    }
  }

  _createArrows() {
    var e = document.createElement('ul');
    e.classList.add('keen-nav');
    e.setAttribute('data-keen-nav', true);
    e.innerHTML = `
        <li>
          <button data-keen-prev class="keen-arrow-prev keen-arrow" aria-label="Previous Slide"></button>
        </li>
        <li>
          <button data-keen-next class="keen-arrow-next keen-arrow" aria-label="Next Slide"></button>
        </li>
    `;
    this._navContainer = e;
    this._prevArrow = this._navContainer.querySelector('[data-keen-prev]');
    this._nextArrow = this._navContainer.querySelector('[data-keen-next]');

    if (this._options.appendArrows) {
      let toAppend = document.querySelector(this._options.appendArrows);
      toAppend.appendChild(e);
    } else {
      this._elem.appendChild(e);
    }
  }

  _createDots() {
    var e = document.createElement('ul');
    e.classList.add('keen-dots');
    e.setAttribute('data-keen-dots', true);

    let dotsToMake = Math.ceil(this._slides.length / this._options.slidesPerView);

    for (let i = 0; i < dotsToMake; i++) {
      e.innerHTML += `
        <li>
          <button data-dot='${i * this._options.slidesPerView}'></button>
        </li>
      `;
    }

    this._elem.classList.add('keen--dotted');
    this._elem.appendChild(e);
  }

  fader(slider) {
    var opacities = slider.details().positions.map(slide => slide.portion);
    for (let [index, slide] of this._slides.entries()) {
      slide.style.opacity = opacities[index];
    }
  }

  goToSlide(index, dontAnimate = false) {
    let duration = dontAnimate ? 0 : 500;

    let nearest = false;

    if (this._options.loop && this._slider.details().size > 2) {
      nearest = true;
    }
    this._slider.moveToSlideRelative(index, nearest, duration);
  }

  goToPrev(e) {
    e.preventDefault();
    this._slider.prev();
  }

  goToNext(e) {
    e.preventDefault();
    this._slider.next();
  }

  scrollRevealFix() {
    for (let slide of this._slides) {
      if (slide.hasAttribute('data-reveal')) {
        slide.style.removeProperty('opacity');
        slide.style.removeProperty('transition');
        slide.style.removeProperty('visibility');
        slide.removeAttribute('data-reveal');
      }
    }
  }

  getActiveSlideElem(slider = this._slider) {
    let slideIndex = slider.details().relativeSlide;

    for (let [index, slide] of this._slides.entries()) {
      if (index === slideIndex) {
        return slide;
      }
    }

    return false;
  }

  adaptiveHeight(slider = this._slider) {
    if (!this._options.adaptiveHeight || !slider) return false;

    this._elem.classList.add('keen--adaptive-height');

    let slide = slider.details().relativeSlide;

    let height = 0;

    height += parseFloat(getComputedStyle(this._elem).paddingBottom);
    height += parseFloat(getComputedStyle(this._elem).paddingTop);

    let activeSlide = this.getActiveSlideElem(slider);

    height += activeSlide.clientHeight;
    this._elem.style.height = `${height}px`;
  }

  updateFadeHeight(slider = this._slider) {
    if (!this._options.fade || !slider) return false;

    let slide = slider.details().relativeSlide;

    let height = 0;

    height += parseFloat(getComputedStyle(this._elem).paddingBottom);
    height += parseFloat(getComputedStyle(this._elem).paddingTop);

    let activeSlide = this.getActiveSlideElem(slider);

    height += activeSlide.getBoundingClientRect().height;

    this._elem.style.height = `${height}px`;

    if (!this._elem.classList.contains('fade-slider')) {
      this._elem.classList.add('fade-slider');
    }
  }

  alignArrows() {
    if (!this._options.alignArrowsTo) return false;
    let elem = this._elem.querySelector(this._options.alignArrowsTo);
    this._navContainer.style.top = `${elem.clientHeight / 2}px`;
  }

  updateClasses(slider) {
    if (!slider) return false;

    let slide = slider.details().relativeSlide;
    let maxSlides = slider.details().size;
    let endSlide = slide + slider.details().slidesPerView;

    if (this._prevArrow && !this._options.loop) {
      if (slide === 0) {
        this._prevArrow.setAttribute('disabled', 'disabled');
      } else {
        this._prevArrow.removeAttribute('disabled');
      }
    }

    if (this._nextArrow && !this._options.loop) {
      if (this.isAtEnd(slider)) {
        this._nextArrow.setAttribute('disabled', 'disabled');
      } else {
        this._nextArrow.removeAttribute('disabled');
      }
    }

    if (this._dots) {
      let dotIndex =
        Math.floor(slide / slider.details().slidesPerView) * slider.details().slidesPerView;

      for (let dot of this._dots) {
        dot.classList.remove('active');

        if (Number(dot.dataset.dot) === dotIndex) {
          dot.classList.add('active');
        }
      }
    }

    let activeSlide = this.getActiveSlideElem(slider);

    for (let slide of this._slides) {
      slide.classList.remove('active');
    }

    activeSlide.classList.add('active');
  }

  isAtEnd(slider = this._slider) {
    let slide = slider.details().relativeSlide;
    let maxSlides = slider.details().size;
    let endSlide = slide + Math.floor(slider.details().slidesPerView);
    return endSlide === maxSlides;
  }

  autoplay() {
    if (!this._options.autoplay) {
      if (this._autoplayInterval) clearInterval(this._autoplayInterval);
      return false;
    }
    clearInterval(this._autoplayInterval);
    this._autoplayInterval = null;

    this._autoplayInterval = setInterval(() => {
      if (this.shouldAutoplay && this._slider) {
        if (this.isAtEnd() && !this._options.loop) {
          this.goToSlide(0);
        } else {
          this._slider.next();
        }
      }
    }, this._options.autoplaySpeed);
  }

  moveFree(amount) {
    let distance = amount - this._lastRangeAmount;
    distance = distance >= 0 ? Math.ceil(distance) : Math.floor(distance);

    let modifier = this._scrollWidth / 100.0;
    distance = distance * modifier;

    console.log(distance);

    this._slider.moveNormally(distance);

    this._lastRangeAmount = amount;
  }

  moveWithSpeed() {
    this._slider.moveWithSpeed();
  }
}
