import Component from '../inherited/Component';
import Helpers from '../lib/Helpers';

let slick = null;
let jQuery = null;

export default class Slider extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._options = {
      ...{
        slidesToShow: 1,
        rows: 0,
        swipeToSlide: true,
        mount: true,
        arrows: false
      },
      ...this._options
    };
    this._isMounted = false;
    this._addedListener = false;
  }

  async mount() {
    if (this._options.enableWhen && !this._addedListener) {
      this._addedListener = true;
      window.addEventListener('resize', this.resizeListener.bind(this), {
        passive: true
      });
    }

    if (
      this._options.enableWhen &&
      matchMedia &&
      !matchMedia(`only screen and ${this._options.enableWhen}`).matches
    ) {
      return false;
    }
    if (this._options.mount === false) {
      return false;
    }

    slick = await import('slick-carousel');
    jQuery = await import('jquery');
    jQuery = jQuery.default;

    jQuery(this._elem).on('init', (e, slider) => {
      let vid = slider.$slides.length ? slider.$slides[0].querySelector('video') : false;
      if (vid) {
        vid.play();
        vid.removeAttribute('poster');
      }

      if (slider.slickGetOption('autoplayOnScroll')) {
        this._setAutoplayOnScroll();
      }
      jQuery.each(slider.$dots, (i, el) => {
        jQuery(el)
          .find('li')
          .eq(slider.currentSlide)
          .addClass('slick-active');
      });
      if (slider.$slides.length) {
        this.addCurrentSlideClass(slider.$slides.get(slider.currentSlide));
      }
    });
    jQuery(this._elem).slick(this._options);

    jQuery(this._elem).on('beforeChange', (event, slick, currentSlide) => {
      if (!slick.$slides[currentSlide]) return false;
      let vid = slick.$slides[currentSlide].querySelector('video');
      if (vid) {
        vid.pause();
      }
    });

    jQuery(this._elem).on('afterChange', (event, slick, currentSlide) => {
      jQuery.each(slick.$dots, (i, el) => {
        jQuery(el)
          .find('li')
          .eq(currentSlide)
          .addClass('slick-active');
      });
      this.addCurrentSlideClass(slick.$slides.get(currentSlide));

      if (!slick.$slides[currentSlide]) return false;
      let vid = slick.$slides[currentSlide].querySelector('video');
      if (vid) {
        vid.play();
        vid.removeAttribute('poster');
      }
    });

    this._ctx.on('refresh-slick', (state, data) => {
      jQuery(this._elem).slick('setPosition');
    });
    this._ctx.emit('slider--ready', null, { id: this._options.id });

    this._isMounted = true;
  }

  isMounted() {
    return this._isMounted;
  }

  addCurrentSlideClass(slideElem) {
    this._elem.classList.remove('has-cta');

    for (let className of slideElem.classList) {
      if (className.includes('style--')) {
        this._elem.setAttribute('data-current-slide-style', className);
      }
    }

    if (slideElem.querySelector('.link-btn')) {
      this._elem.classList.add('has-cta');
    }
  }

  resizeListener() {
    if (matchMedia(`only screen and ${this._options.enableWhen}`).matches && !this._isMounted) {
      this.mount();
    } else if (
      !matchMedia(`only screen and ${this._options.enableWhen}`).matches &&
      this._isMounted
    ) {
      this.unmount();
      this._isMounted = false;
    }
  }

  _setAutoplayOnScroll() {
    requestAnimationFrame(this._doAutoplayCalculations.bind(this));
  }
  _doAutoplayCalculations() {
    if (
      Helpers.isAnyPartOfElementInViewport(this._elem) &&
      jQuery(this._elem).slick('slickGetOption', 'autoplay') === false
    ) {
      jQuery(this._elem).slick('slickSetOption', 'autoplay', true, true);
      jQuery(this._elem).slick('slickPlay');
      let slider = jQuery(this._elem).slick('getSlick');
      jQuery.each(slider.$dots, (i, el) => {
        jQuery(el)
          .find('li')
          .eq(slider.currentSlide)
          .addClass('slick-active');
      });
    } else if (
      !Helpers.isAnyPartOfElementInViewport(this._elem) &&
      jQuery(this._elem).slick('slickGetOption', 'autoplay') === true
    ) {
      jQuery(this._elem).slick('slickSetOption', 'autoplay', false, true);
      jQuery(this._elem).slick('slickPause');
      let slider = jQuery(this._elem).slick('getSlick');
      jQuery.each(slider.$dots, (i, el) => {
        jQuery(el)
          .find('li')
          .eq(slider.currentSlide)
          .addClass('slick-active');
      });
    }
    requestAnimationFrame(this._doAutoplayCalculations.bind(this));
  }

  unmount() {
    if (jQuery(this._elem).slick('getSlick')) {
      jQuery(this._elem).slick('unslick');
      window.removeEventListener('resize', this.resizeListener);
    }
  }

  getSpeed() {
    return parseFloat(jQuery(this._elem).slick('slickGetOption', 'speed'));
  }

  goToSlide(i, dontAnimate = false) {
    jQuery(this._elem).slick('slickGoTo', i, dontAnimate);
  }
}
