import Component from '../inherited/Component'
import Helpers from '../lib/Helpers'

export default class Header extends Component {
  constructor (elem, theme, options, ctx) {
    super(elem, theme, options, ctx)
    this._lastScrollY = 0
    this._currentScrollY = window.scrollY
    this._currentDirection = 'down'
    this._headerHeight = this._elem.getBoundingClientRect().height
  }

  mount () {
    this._assignVariables()
    window.addEventListener('scroll', e => {
      this._assignVariables()
    }, { passive: true })
    requestAnimationFrame(this._doScrollCalculations.bind(this))
  }

  _doScrollCalculations () {
    if (this._currentDirection === 'down' && this._currentScrollY >= this._headerHeight) {
      this._elem.classList.add('not-visible')
      Helpers.nextFrame(e => {
        this._elem.classList.add('fixed')
      })
    }

    if (this._currentDirection === 'up') {
      this._elem.classList.remove('not-visible')
    }

    if (this._currentScrollY === 0) {
      this._elem.classList.remove('fixed')
    }

    if (document.body.classList.contains('no-scroll')) {
      this._elem.classList.remove('not-visible')
      this._elem.classList.add('fixed')
    }

    requestAnimationFrame(this._doScrollCalculations.bind(this))
  }

  _assignVariables () {
    this._currentScrollY = window.scrollY
    this._currentDirection = this._currentScrollY > this._lastScrollY ? 'down' : 'up'
    this._lastScrollY = window.scrollY
  }
}