import Component from '../inherited/Component';
import Helpers from '../lib/Helpers';

export default class StoreLocator extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._map = document.querySelector('#bh-sl-map-container');
    this._input = document.querySelector('#bh-sl-address');
  }

  async mount() {
    if (Helpers.getParameterByName('bh-sl-address')) {
      this._map.classList.add('always-shown');

      this._input.value = Helpers.getParameterByName('bh-sl-address');

      if (document.querySelector('#bh-sl-map iframe')) {
        this.run();
      } else {
        let i = 0;
        let interval = setInterval(() => {
          if (document.querySelector('#bh-sl-map iframe')) {
            this.run();
            clearInterval(interval);
          }
          if (i > 100) {
            clearInterval(interval);
          }
          i++;
        }, 50);
      }
    }
  }

  run() {
    setTimeout(() => {
      let button = document.querySelector('#bh-sl-submit');
      button.click();
    }, 100);
  }
}
