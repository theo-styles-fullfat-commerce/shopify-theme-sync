import Component from '../inherited/Component';
import Helpers from '../lib/Helpers';

export default class SlideToggle extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
  }

  mount() {
    if (this._options.length) {
      for (let option of this._options) {
        this._optionLogic(option);
      }
    } else {
      this._optionLogic(this._options);
    }
  }

  _optionLogic(option) {
    this._elem.addEventListener(option.on, async e => {
      if (option.preventDefault) {
        e.preventDefault();
      }

      let target = document.querySelector(option.selector);
      let method = null;

      if (option.method === 'slideDown') {
        method = '_slideDown';
      } else if (option.method === 'slideUp') {
        method = '_slideUp';
      } else {
        method = this._elem.classList.contains('open') ? '_slideUp' : '_slideDown';
      }

      if (option.useQueue) {
        this._ctx.getState().eventQueue.add(async () => {
          return await this[method](target);
        });
      } else {
        await this[method](target);
      }
    });
  }

  _slideDown(elem) {
    return new Promise((resolve, reject) => {
      elem.style.maxHeight = 'none';
      let scrollHeight = elem.scrollHeight;
      elem.style.maxHeight = '0px';
      this._elem.classList.add('open');
      elem.dataset.transitioning = true;

      let openEvent = e => {
        if (e.propertyName === 'max-height') {
          elem.dataset.transitioning = false;
          elem.style.maxHeight = 'none';
          elem.classList.add('open');
          elem.removeEventListener('transitionend', openEvent, false);
          resolve();
        }
      };
      elem.addEventListener('transitionend', openEvent, false);
      Helpers.nextFrame(() => {
        elem.style.maxHeight = `${scrollHeight}px`;
      });
    });
  }

  _slideUp(elem) {
    return new Promise((resolve, reject) => {
      elem.style.maxHeight = `${elem.scrollHeight}px`;
      this._elem.classList.remove('open');
      elem.dataset.transitioning = true;

      let closeEvent = e => {
        if (e.propertyName === 'max-height') {
          elem.removeEventListener('transitionend', closeEvent, false);
          elem.dataset.transitioning = false;
          resolve();
        }
      };
      elem.addEventListener('transitionend', closeEvent, false);
      Helpers.nextFrame(() => {
        elem.style.maxHeight = `0px`;
        elem.classList.remove('open');
      });
    });
  }
}
