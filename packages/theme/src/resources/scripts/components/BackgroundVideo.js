import Component from '../inherited/Component';

export default class BackgroundVideo extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._loaded = false;
  }

  mount() {
    window.addEventListener('load', async e => {
      this._loadVideo();
    });

    window.addEventListener('resize', async e => {
      this._loadVideo();
    });
  }

  _loadVideo() {
    const bgv = this._elem;
    const visible = bgv.offsetParent;

    if (visible) {
      const children = bgv.getElementsByTagName('source');

      for (let i = 0; i < children.length; ++i) {
        children[i].src = children[i].dataset.src;
      }
    }

    if (!this._loaded) {
      bgv.load();
      this._loaded = true;
    }
  }
}
