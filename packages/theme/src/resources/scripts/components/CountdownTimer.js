import Helpers from '../lib/Helpers';
import AjaxApi from '../lib/AjaxApi';
import Component from '../inherited/Component';

export default class CountdownTimer extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._elem = elem;
    this._options = {
      ...{
        epoch: 0,
        seperator: ':'
      },
      ...this._options
    };
    this._timer = null;
    this._timerElems = this._elem.querySelectorAll('[data-time]');
  }

  async mount() {
    let Countdown = await import('countdown-js');

    this._timer = Countdown.timer(
      new Date(this._options.epoch * 1000),
      timeLeft => {
        this._updateTimeElems(timeLeft);
      },
      () => {
        this._updateTimeElems(
          {
            days: 0,
            hours: 0,
            minutes: 0,
            seconds: 0
          },
          false
        );
      }
    );
  }

  _updateTimeElems(timeLeft, reachedZero = true) {
    for (let [index, timeElem] of this._timerElems.entries()) {
      let time = timeLeft[timeElem.dataset.time];
      if (time === 0 && reachedZero) {
        timeElem.classList.add('hide');
      } else {
        reachedZero = false;
        timeElem.textContent = Helpers.pad(time, 2);

        if (!this._options.numbersOnly) {
          if (timeElem.dataset.time === 'days') {
            timeElem.textContent += 'd';
          }

          if (timeElem.dataset.time === 'hours') {
            timeElem.textContent += 'h';
          }

          if (timeElem.dataset.time === 'minutes') {
            timeElem.textContent += 'm';
          }

          if (timeElem.dataset.time === 'seconds') {
            timeElem.textContent += 's';
          }
        }

        if (index + 1 !== this._timerElems.length) {
          timeElem.textContent += this._options.seperator;
        }

        if (
          timeLeft.days === 0 &&
          timeLeft.hours === 0 &&
          timeLeft.minutes === 0 &&
          timeLeft.seconds === 0
        ) {
          this._elem.classList.add('hide');
        }
      }
    }
  }
}
