import Component from '../inherited/Component';
import Helpers from '../lib/Helpers';
import axios from 'axios';

export default class KlaviyoForm extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._email = this._elem.querySelector('[data-email]');
    this._fields = this._elem.querySelectorAll('[data-field]');
    this._button = this._elem.querySelector('[type="submit"]');
    this._callbacks = [];
  }

  async mount() {
    this.querystring = await import('querystring');

    this._elem.addEventListener('submit', e => {
      e.preventDefault();

      console.log('SUBMIT');

      let valid = this._elem.reportValidity();
      if (valid) {
        this._elem.setAttribute('disabled', 'disabled');
        let data = {
          g: this._options.listId,
          email: this._email.value,
          $list_fields: '$consent',
          $consent: 'email',
          $source: this._options.source
        };
        let fields = [];
        for (let fieldElem of this._fields) {
          if (fieldElem.value.trim().length === 0) continue;
          data[fieldElem.dataset.field] = fieldElem.value.trim();
          fields.push(fieldElem.dataset.field);
        }
        if (fields.length) {
          data['$fields'] = fields.join(',');
        }
        data = this.querystring.stringify(data);
        axios
          .post(this._elem.getAttribute('action'), data, {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Access-Control-Allow-Headers': '*',
              'Access-Control-Allow-Origin': '*'
            }
          })
          .then(response => {
            this._elem.removeAttribute('disabled');
            if (response.data.success) {
              this._elem.classList.add('submitted');
              this._elem.innerHTML = `<div class="rte">${this._options.newsletter_success}</div>`;

              for (let cb of this._callbacks) {
                cb();
              }
            }
          });
      }
    });
  }

  addSubmitCallback(cb) {
    this._callbacks.push(cb);
  }
}
