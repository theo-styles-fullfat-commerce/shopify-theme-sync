import Component from '../inherited/Component';
import LazyLoad from 'vanilla-lazyload';

export default class LazyLoadComp extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._options = {
      ...{
        elements_selector: '[data-lazy]',
        callback_loaded: e => {
          if (e.parentNode.nodeName === 'PICTURE') {
            e.parentNode.classList.add('loaded');
          }
          if (e.nodeName === 'VIDEO') {
            this._ctx.emit('ffc--video-loaded', null, e);
          }
        },
        callback_error: e => {
          if (e.parentNode.nodeName === 'PICTURE') {
            e.parentNode.classList.add('error');
          }
        },
        threshold: 500
      },
      ...this._options
    };
    this._lazyLoad = null;
  }

  mount() {
    this._lazyLoad = new LazyLoad(this._options);

    this._ctx.on('ffc--lazy-load-update', e => {
      this._lazyLoad.update();
    });

    this._ctx.on('ffc--lazy-load-elem', (state, elem) => {
      LazyLoad.load(elem, this._options);
    });

    document.addEventListener('ffc--lazy-load-update', e => {
      this._lazyLoad.update();
    });
  }
}
