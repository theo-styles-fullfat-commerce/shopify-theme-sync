import Component from '../inherited/Component';
import Helpers from '../lib/Helpers';

let OverlayScrollBar = null;

export default class DynamicScroller extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._options = {
      ...{
        className: 'os-theme-dark',
        callbacks: {
          onInitialized: e => {
            this._ctx.emit('ffc--lazy-load-update');
            this._ctx.emit('ffc--dynamic-scoller--ready');
          }
        },
        updateOnLoad: [],
        sizeAutoCapable: true,
        scrollbars: {
          clickScrolling: true
        },
        cols: null,
        type: 'horizontal'
      },
      ...this._options
    };
    this._scrollBar = null;
    this._children = this._elem.querySelectorAll('[data-scroll-item]');
    this._isMounted = false;
    this._addedListener = false;
    this.Hammer = null;
  }

  async mount() {
    if (this._options.enableWhen) {
      if (!this._addedListener) {
        window.addEventListener(
          'resize',
          e => {
            if (
              matchMedia(`only screen and ${this._options.enableWhen}`).matches &&
              !this._isMounted
            ) {
              this.mount();
              this._isMounted = true;

              Helpers.nextFrame(() => {
                if (this._scrollBar) this._scrollBar.update(true);
              });
            } else if (
              !matchMedia(`only screen and ${this._options.enableWhen}`).matches &&
              this._isMounted
            ) {
              this.unmount();
              this._isMounted = false;
            }
          },
          { passive: true }
        );
        this._addedListener = true;
      }
      if (!matchMedia(`only screen and ${this._options.enableWhen}`).matches) {
        return false;
      }
    }

    this._isMounted = true;

    OverlayScrollBar = await import('overlayscrollbars');
    OverlayScrollBar = OverlayScrollBar.default;

    console.log(this._children);

    this._children = this._elem.querySelectorAll('[data-scroll-item]');

    if (
      this._options.cols &&
      this._children.length > this._options.cols &&
      this._options.type === 'horizontal'
    ) {
      for (let child of this._children) {
        child.style.width = `calc((100% - 30px) / ${this._options.cols})`;
        child.style.minWidth = `calc((100% - 30px) / ${this._options.cols})`;
      }
      this._scrollBar = new OverlayScrollBar(this._elem, this._options);
      this._elem.classList.add('scroller--init');
    } else {
      this._scrollBar = new OverlayScrollBar(this._elem, this._options);
      this._elem.classList.add('scroller--init');
    }

    if (matchMedia('only screen and (min-width: 835px)').matches) {
      this.Hammer = await import('hammerjs');
      this.Hammer = this.Hammer.default;
      this._attachEvent();
    }
  }

  _setWidths() {
    for (let child of this._children) {
      child.style.width = `calc((100% - 30px) / ${this._options.cols})`;
      child.style.minWidth = `calc((100% - 30px) / ${this._options.cols})`;
    }
    if (this._scrollBar !== null) {
      this._scrollBar.update();
    }
  }

  scroll(...args) {
    this._scrollBar.scroll(...args);
  }

  _attachEvent() {
    this._elem.classList.add('dragging--enabled');

    let oldX = 0;
    let newX = 0;
    let calc = 0;
    let friction = 0.025;

    let applyVelocity = (v, dir) => {
      var dist = v * 5;

      if (dir === this.Hammer.DIRECTION_RIGHT) {
        dist *= -1;
      }

      if (dist !== 0) {
        this._moveScrollBar(dist);
      }

      if (v > 0) {
        v -= friction;
        window.requestAnimationFrame(() => {
          applyVelocity(v, dir);
        });
      }
    };

    this._mc = new this.Hammer(this._elem);

    this._mc.add(
      new this.Hammer.Pan({
        direction: this.Hammer.DIRECTION_HORIZONTAL,
        threshold: 0
      })
    );

    this._mc.on('panstart', ev => {
      oldX = ev.center.x;
      this._elem.classList.add('scroll--moving');
    });

    this._mc.on('pan', ev => {
      newX = ev.center.x;
      calc = oldX - newX;
      oldX = newX;

      if (calc !== 0) {
        this._moveScrollBar(calc);
      }

      if (ev.isFinal) {
        this._elem.classList.remove('scroll--moving');
        if (ev.velocityX < 0) {
          ev.velocityX *= -1;
        }
        applyVelocity(ev.velocityX, ev.direction);
      }
    });
  }

  unmount() {
    for (let child of this._children) {
      child.removeAttribute('style');
    }
    if (this._scrollBar) this._scrollBar.destroy();
    if (this._mc) {
      this._mc.off('panstart');
      this._mc.off('pan');
    }
  }

  _moveScrollBar(calc) {
    this._scrollBar.scroll({ x: `+= ${calc}` });
  }
}
