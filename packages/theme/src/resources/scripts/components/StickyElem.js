import Component from '../inherited/Component';

let hcSticky = null;

export default class StickyElem extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._sticky = null;
    this._options = options;
    this._selector = this._options.top;
  }

  async mount() {
    hcSticky = await import('hc-sticky');
    hcSticky = hcSticky.default;

    if (typeof this._selector === 'string') {
      this._options.top = document.querySelector(this._selector).getBoundingClientRect().height;
    }
    this._sticky = new hcSticky(this._elem, this._options);

    if (typeof this._selector === 'string') {
      window.addEventListener(
        'resize',
        e => {
          this._options.top = document.querySelector(this._selector).getBoundingClientRect().height;
          this._sticky.update(this._options);
        },
        { passive: true }
      );
    }
  }
}
