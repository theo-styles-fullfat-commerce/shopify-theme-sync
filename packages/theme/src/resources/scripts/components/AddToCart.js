import Component from '../inherited/Component';
import AjaxApi from '../lib/AjaxApi';

export default class AddToCart extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._options = {
      ...{
        redirect: null,
        addingText: 'Adding...',
        addedText: 'Added to bag!',
        waitFor: 1500
      },
      ...this._options
    };
    this._variantIdElem = this._elem.querySelector('[name="id"]');
    this._sellingPlanIdElems = this._elem.querySelectorAll('[name="selling_plan"]');

    this._quantityElem = this._elem.querySelector('[name="quantity"]');
    this._submitElems =
      this._elem.nodeName === 'FORM'
        ? this._elem.querySelectorAll('[type="submit"]')
        : [this._elem];
    this._propertyElems = this._elem.querySelectorAll('[name^="properties"]');

    this._extraProductsInput = this._elem.querySelector('[data-extra-products]');

    this._globalExtraProducts = document.querySelector('[data-extra-atc-products]')
      ? JSON.parse(document.querySelector('[data-extra-atc-products]').innerHTML)
      : [];
  }

  mount() {
    this._addListeners();
  }

  _getProperties() {
    let properties = {};
    let propElems = Array.from(this._propertyElems).filter(
      elem => !elem.hasAttribute('disabled') && elem.value.length > 0
    );
    for (let elem of propElems) {
      properties[elem.getAttribute('name').match(/^properties\[(.+)\]$/)[1]] = elem.value;
    }

    if (propElems.length === 0 && this._elem.dataset.properties) {
      properties = JSON.parse(this._elem.dataset.properties);
    }

    return properties;
  }

  _addListeners() {
    let event = null;
    if (this._elem.nodeName === 'FORM') {
      event = 'submit';
    } else {
      event = 'click';
    }
    this._elem.addEventListener(event, async e => {
      e.preventDefault();
      //let originalText = this._submitElem.innerHTML
      //this._submitElem.innerHTML = this._options.addingText
      for (let elem of this._submitElems) {
        elem.classList.add('ffc--adding-to-cart');
      }

      let variantId = this._variantIdElem
        ? this._variantIdElem.value
        : this._elem.dataset.variantId;
      let quantity = this._quantityElem
        ? Number(this._quantityElem.value)
        : Number(this._elem.dataset.quantity);
      let selling_plan = this._elem.dataset.sellingPlan || '';

      if (this._sellingPlanIdElems) {
        for (let elem of this._sellingPlanIdElems) {
          if (elem.checked) {
            selling_plan = elem.value;
          }
        }
      }

      let properties = this._getProperties();

      let items = [
        {
          id: variantId,
          quantity: quantity,
          properties: properties,
          selling_plan: selling_plan
        }
      ];

      // Add extra products to every atc (if quantity is less)
      if (this._globalExtraProducts.length) {
        let cart = await AjaxApi.getCart();
        let globalProductsToAdd = this._globalExtraProducts
          .filter(product => {
            let item = cart.items.find(item => `${item.variant_id}` === `${product.id}`);
            if (item && item.quantity >= product.quantity) {
              return false;
            }
            return true;
          })
          .map(product => {
            let item = cart.items.find(item => `${item.variant_id}` === `${product.id}`);
            if (item && item.quantity < product.quantity) {
              product.quantity -= item.quantity;
            }
            return product;
          });

        items = [...globalProductsToAdd, ...items];
      }

      if (this._extraProductsInput && this._extraProductsInput.value) {
        let extraProducts = JSON.parse(this._extraProductsInput.value);

        for (let prod of extraProducts) {
          items.push({
            id: prod.id,
            quantity: Number(prod.quantity || 1),
            properties: prod.properties || {},
            selling_plan: prod.selling_plan
          });
        }
      }

      let data = await AjaxApi.addToCart({
        items: items
      });

      for (let item of data.items) {
        let matching = items.find(it => `${it.id}` === `${item.variant_id}`);
        this._ctx.emit('cart-atc', null, {
          item: item,
          quantity: matching.quantity,
          properties: matching.properties
        });
      }

      this._ctx.emit('cart-item-added', {
        lastItemAdded: {
          item: data,
          quantity: quantity,
          properties: properties
        }
      });

      document.dispatchEvent(
        new CustomEvent('ffc--cart-item-added', {
          detail: { product: data.items[0], quantity: quantity }
        })
      );

      if (!this._options.redirect) {
        for (let elem of this._submitElems) {
          elem.classList.remove('ffc--adding-to-cart');
          elem.classList.add('ffc--added-to-cart');
        }
        //this._submitElem.innerHTML = this._options.addedText

        setTimeout(() => {
          //this._submitElem.innerHTML = originalText
          for (let elem of this._submitElems) {
            elem.classList.remove('ffc--added-to-cart');
          }
        }, this._options.waitFor);
      }

      if (this._options.redirect) {
        window.location = this._options.redirect;
      }
    });
  }
}
