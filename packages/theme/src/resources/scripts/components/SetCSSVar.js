import Component from '../inherited/Component'

export default class SetCSSVar extends Component {
  constructor (elem, theme, options, ctx) {
    super(elem, theme, options, ctx)
  }

  mount () {
    if (this._options.length) {
      for (let option of this._options) {
        this._optionLogic(option)

        if (option.updateOnResize === true) {
          window.addEventListener('resize', e => {
            this._optionLogic(option)
          }, { passive: true })
        }

      }
    } else {
      this._optionLogic(this._options)

      if (this._options.updateOnResize === true) {
        window.addEventListener('resize', e => {
          this._optionLogic(this._options)
        }, { passive: true })
      }
    }
  }

  _optionLogic(option) {
    let toGet = document.querySelector(option.selector)
    let dimensions = toGet.getBoundingClientRect()

    document.documentElement.style.setProperty(option.var,`${dimensions[option.get]}px`)
  }
}