import Component from '../inherited/Component';

export default class NewsletterPopup extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._closeElem = this._elem.querySelector('[data-close]');
    this._timeToAdd = 1000 * 60 * 60 * 24 * 7;
    this._timeToAddSubmit = 1000 * 60 * 60 * 24 * 7 * 52;
    this._formKlaviyo = this._elem.querySelector('[data-klaviyo-form]');
    this._form = this._elem.querySelector('form');
    this._storageKey = 'FFC_NEWSLETTER_CLOSED';
  }

  mount() {
    let epoch = localStorage.getItem(this._storageKey)
      ? Number(localStorage.getItem(this._storageKey))
      : false;
    let now = new Date().getTime();
    let difference = epoch - now;

    if (!epoch || difference <= 0) {
      this._elem.classList.add('shown');
      document.body.classList.add('ffc--newsletter--open');
    }

    this._closeElem.addEventListener('click', e => {
      e.preventDefault();
      this._close();
    });

    this._ctx.on('ffc--mounted', () => {
      if (this._formKlaviyo) {
        this._formComponent = this._formKlaviyo
          ? this._theme.getComponentByElement(this._formKlaviyo).component
          : null;
        this._formComponent.addSubmitCallback(() => {
          now = new Date().getTime();
          localStorage.setItem(this._storageKey, now + this._timeToAddSubmit);
        });
      } else if (this._form) {
        let that = this;
        this._form.addEventListener('submit', function(e) {
          that._close();
        });
      }
    });
  }

  _close() {
    console.log('+++++++++');
    let now = new Date().getTime();
    localStorage.setItem(this._storageKey, now + this._timeToAdd);
    this._elem.classList.remove('shown');
    document.body.classList.remove('ffc--newsletter--open');
  }
}
