import Component from '../inherited/Component'

export default class SetHTML extends Component {
  constructor (elem, theme, options, ctx) {
    super(elem, theme, options, ctx)
  }

  mount () {
    if (this._options.length) {
      for (let option of this._options) {
        this._optionLogic(option)
      }
    } else {
      this._optionLogic(this._options)
    }
  }

  _optionLogic(option) {
    this._elem.addEventListener(option.on, async e => {
      if (option.preventDefault) {
        e.preventDefault()
      }

      if (option.onlyThisElem && e.target !== this._elem) {
        return false
      }

      if (option.target.includes('closest:')) {
        let parentSelector = option.target.split('closest:')[1].trim()
        if (option.subTarget) {
          this._elem.closest(parentSelector).querySelector(option.subTarget).innerHTML = option.content
        } else {
          this._elem.closest(parentSelector).innerHTML = option.content
        }
      } else if (option.target === 'this') {
        this._elem.innerHTML = option.content
      } else {
        let targets = document.querySelectorAll(option.target)
        for (let target of targets) {
          target.innerHTML = option.content
        }
      }
    
    })
  }
}