import Component from '../inherited/Component'

export default class MethodCall extends Component {
  constructor (elem, theme, options, ctx) {
    super(elem, theme, options, ctx)
  }

  mount () {
    if (this._options.length) {
      for (let option of this._options) {
        this._optionLogic(option)
      }
    } else {
      this._optionLogic(this._options)
    }
  }

  _optionLogic(option) {
    this._elem.addEventListener(option.on, async e => {
      if (option.preventDefault) {
        e.preventDefault()
      }

      if (option.onlyThisElem && e.target !== this._elem) {
        return false
      }
      
      let comp = this._theme.getComponent(option.componentId).component
      comp[option.method](...option.args)
    })
  }
}