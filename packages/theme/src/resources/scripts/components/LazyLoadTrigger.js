import Component from '../inherited/Component';

export default class LazyLoadTrigger extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
  }

  mount() {
    if (this._options.length) {
      for (let option of this._options) {
        this._optionLogic(option);
      }
    } else {
      this._optionLogic(this._options);
    }
  }

  _optionLogic(option) {
    if (!option.on) {
      option.on = 'click';
    }

    if (option.whenOutside) {
      document.addEventListener(
        option.on,
        e => {
          let clickIsInside = this._elem.contains(e.target) || this._elem === e.target;
          let toExclude = document.querySelectorAll(option.whenOutsideExcept);
          let hasClickedExclude = Array.from(toExclude).some(
            elem => elem.contains(e.target) || elem === e.target
          );

          if (clickIsInside || hasClickedExclude) return false;

          this._eventLogic(option, e);
        },
        option.preventDefault ? { passive: false } : { passive: true }
      );
    } else {
      this._eventListener = e => {
        this._eventLogic(option, e);
      };
      this._elem.addEventListener(
        option.on,
        this._eventListener,
        option.preventDefault ? { passive: false } : { passive: true }
      );
    }
  }

  _lazyLoadElem(option, elem) {
    this._ctx.emit('ffc--lazy-load-elem', null, elem);
    if (this._eventListener) {
      this._elem.removeEventListener(option.on, this._eventListener);
    }
  }

  _eventLogic(option, e) {
    if (option.onlyThisElem && e.target !== this._elem) {
      return false;
    }

    if (option.preventDefault) {
      e.preventDefault();
    }

    let elem = null;

    if (option.target.includes('closest:')) {
      let parentSelector = option.target.split('closest:')[1].trim();
      if (option.subTarget) {
        elem = this._elem.closest(parentSelector).querySelector(option.subTarget);
      } else {
        elem = this._elem.closest(parentSelector);
      }
      this._lazyLoadElem(option, elem);
    } else if (option.target === 'this') {
      elem = this._elem;
      if (option.subTarget) {
        elem = this._elem.querySelector(option.subTarget);
      }
      this._lazyLoadElem(option, elem);
    } else {
      let targets = document.querySelectorAll(option.target);
      for (let target of targets) {
        this._lazyLoadElem(option, target);
      }
    }
  }
}
