import Component from '../inherited/Component';

class MegaMenu extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._options = {
      ...{
        historyState: false
      },
      ...this._options
    };
    this._navItems = this._elem.querySelectorAll('[data-nav-item]');
    this._megaMenus = this._elem.querySelectorAll('[data-mega-menu]');
    this._timeout = null;
  }

  mount() {
    for (let navItem of this._navItems) {
      navItem.addEventListener('mouseenter', e => {
        if (this._timeout) {
          clearTimeout(this._timeout);
          this._timeout = null;
        }
        for (let megaMenu of this._megaMenus) {
          megaMenu.classList.remove('open');
        }
        let megaMenu = navItem.nextElementSibling;
        if (megaMenu.hasAttribute('data-mega-menu')) {
          megaMenu.classList.add('open');
          navItem.classList.add('dropdown-open');
        }
      });

      navItem.addEventListener('mouseleave', e => {
        this._timeout = setTimeout(() => {
          let megaMenu = navItem.nextElementSibling;
          if (megaMenu.hasAttribute('data-mega-menu')) {
            megaMenu.classList.remove('open');
            navItem.classList.remove('dropdown-open');
          }
        }, 500);
      });
    }

    for (let megaMenu of this._megaMenus) {
      let megaMenuInner = megaMenu.querySelector('[data-mega-menu-inner]');
      megaMenuInner.addEventListener('mouseenter', e => {
        if (this._timeout) {
          clearTimeout(this._timeout);
          this._timeout = null;
        }
        let navItem = megaMenu.previousElementSibling;
        megaMenu.classList.add('open');
        navItem.classList.add('dropdown-open');
      });

      megaMenuInner.addEventListener('mouseleave', e => {
        let navItem = megaMenu.previousElementSibling;
        megaMenu.classList.remove('open');
        navItem.classList.remove('dropdown-open');
      });
    }
  }
}

export default MegaMenu;
