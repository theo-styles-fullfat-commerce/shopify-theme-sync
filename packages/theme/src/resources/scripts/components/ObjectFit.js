import Component from '../inherited/Component';

export default class ObjectFit extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
  }

  async mount() {
    let objectFitImages = await import('object-fit-images');
    objectFitImages.default();
  }
}
