import Component from '../inherited/Component';
import AjaxApi from '../lib/AjaxApi';
import Helpers from '../lib/Helpers';
import axios from 'axios';

class BoxBuilder extends Component {
  constructor(elem, theme, options, ctx) {
    super(elem, theme, options, ctx);
    this._data = {
      loading: false,
      products: [],
      quickViewOpen: false,
      quickViewLoading: true,
      mobileSummaryOpen: false,
      boxSlots: [],
      errorMessage: ''
    };
    this._jsonElem = this._elem.querySelector('[data-box-builder-json]');
    this._quickViewPopup = this._elem.querySelector('[data-quick-view-popup]');
    this._quickViewPopupContent = this._elem.querySelector('[data-quick-view-popup-content]');
    this._quickViewPopupScroll = this._elem.querySelector('[data-quick-view-popup-scroll]');
    this._mobileSummary = this._elem.querySelector('[data-mobile-summary]');
    this._rivets = null;
  }

  _binders() {
    this._rivets.formatters.sortedBoxSlots = boxSlots => {
      let slotsCopy = [...boxSlots];
      slotsCopy.sort((x, y) => {
        if (x.product && !y.product) {
          return -1;
        }
        if (y.product && !x.product) {
          return 1;
        }
        return 0;
      });
      return slotsCopy;
    };

    this._rivets.formatters.getSizedImage = (src, size) => {
      return src ? Helpers.getSizedImageUrl(src, size) : '';
    };

    this._rivets.formatters.getSrcSet = (src, size) => {
      return src
        ? `${Helpers.getSizedImageUrl(src, size)} 1x, ${Helpers.getSizedImageUrl(
            src,
            size + '@2x'
          )} 2x`
        : '';
    };

    this._rivets.formatters.money = value => {
      return Helpers.formatMoney(value, this._ctx.getState().moneyFormat);
    };

    this._rivets.formatters.length = arr => {
      return arr ? arr.length : 0;
    };

    this._rivets.formatters.eq = (arg1, arg2) => {
      return arg1 === arg2;
    };

    this._rivets.formatters.not = cond => {
      return !cond;
    };

    this._rivets.formatters.append = (str1, str2) => {
      return `${str1}${str2}`;
    };

    this._rivets.formatters.boxFilled = arr => {
      return arr.every(item => item && item.product);
    };

    this._rivets.formatters.showFrequencies = (buyType, boxSlots) => {
      return buyType === 'subscribe' && this._rivets.formatters.boxFilled(boxSlots);
    };

    this._rivets.formatters.pluralize = (count, one, multiple) => {
      return count === 1 ? one : multiple;
    };

    this._rivets.formatters.isInBox = (product, boxSlots) => {
      return boxSlots.find(item => item.product && item.product.handle === product.handle);
    };

    this._rivets.formatters.cheapestPrice = () => {
      let price = this.getPriceForProduct(this._data.products[0]);

      for (let product of this._data.products) {
        let otherPrice = this.getPriceForProduct(product);
        if (otherPrice < price) {
          price = otherPrice;
        }
      }

      return price;
    };

    this._rivets.formatters.fullPrice = (buyType, arr, period) => {
      let filled = this._rivets.formatters.boxFilled(arr);

      if (filled) {
        return arr.reduce((acc, item) => {
          return acc + this.getPriceForProduct(item.product);
        }, 0);
      } else {
        return this._rivets.formatters.cheapestPrice() * this._data.limit;
      }
    };

    this._rivets.formatters.priceText = buyType => {
      let price = this._rivets.formatters.cheapestPrice(buyType);
      return this._data.priceText.replace(
        '[price]',
        `<span class="money">${Helpers.formatMoney(price, this._ctx.getState().moneyFormat)}</span>`
      );
    };

    this._rivets.formatters.addedCount = (product, boxSlots) => {
      return `${
        boxSlots.filter(item => item.product && item.product.handle === product.handle).length
      }x`;
    };

    this._rivets.formatters.totalAddedCount = boxSlots => {
      return this._data.boxSlots.filter(item => {
        return item.product;
      }).length;
    };

    this._rivets.formatters.addedAll = boxSlots => {
      return boxSlots.filter(item => item.product).length === this._data.limit;
    };

    this._rivets.formatters.getQuantityText = quantity => {
      let count = quantity * this._data.limit;
      let plural = count === 1 ? this._data.singularItem : this._data.pluralItem;
      return `(${count} ${plural})`;
    };

    this._rivets.formatters.getProps = item => {
      let props = [];
      for (let key in item.properties) {
        let name = key.charAt(0).toUpperCase() + key.slice(1);
        props.push({ name: name, value: item.properties[key] });
      }
      return props;
    };
  }

  getPriceForProduct(product) {
    let price = product.price;
    let period = this._data.selectedPeriod || 0;
    if (this._data.buyType == 'subscribe' && product.selling_plan_groups.length) {
      price = product.variants[0].selling_plan_allocations[period].price;
    }
    return price;
  }

  async mount() {
    let [rivets, data] = await Promise.all([
      import('rivets'),
      this._prepareData({
        ...this._data,
        ...JSON.parse(this._jsonElem.innerHTML)
      })
    ]);

    this._rivets = rivets;
    this._data = data;

    for (let i = 0; i < this._data.limit; i++) {
      this._data.boxSlots.push({
        product: null
      });
    }

    this._binders();

    this._rivets.bind(this._elem, this._getModel());

    this._elem.classList.add('rendered');

    let { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } = await import(
      'body-scroll-lock'
    );
    this._disableBodyScroll = disableBodyScroll;
    this._enableBodyScroll = enableBodyScroll;
    this._clearAllBodyScrollLocks = clearAllBodyScrollLocks;
  }

  async _prepareData(data) {
    // Make sure products are in stock first
    //data.products = data.products.filter(p => p.available)
    data.priceVaries = false;
    data.products = data.products.filter(item => !item.error);

    if (!data.products.length) return;

    // Base plans and subscription pricing on the first product
    let firstProduct = data.products[0];

    if (firstProduct) {
      let variant = firstProduct.variants[0];

      if (firstProduct.selling_plan_groups.length) {
        let price_adjustment =
          firstProduct.selling_plan_groups[0].selling_plans[0].price_adjustments[0];

        data.subscriptionSaving =
          price_adjustment.value_type === 'percentage'
            ? `${price_adjustment.value}%`
            : `${Helpers.formatMoney(price_adjustment.value, this._ctx.getState().moneyFormat)}`;

        data.plans = firstProduct.selling_plan_groups[0].selling_plans.map((item, index) => {
          return {
            id: item.id,
            title: item.name.replace('Delivery every ', ''),
            index: index
          };
        });
      }
    }

    let price = data.products[0].price;

    for (let product of data.products) {
      product.url = `/products/${product.handle}`;

      if (product.price !== price) {
        data.priceVaries = true;
      }
    }

    console.log(data);

    return data;
  }

  _getModel() {
    return {
      data: this._data,
      controller: {
        addToBox: (e, model) => {
          if (this._boxCount() >= this._data.limit) {
            return false;
          }

          for (let [i, slot] of this._data.boxSlots.entries()) {
            if (!slot || !slot.product) {
              this._data.boxSlots.splice(i, 1, {
                product: model.product
              });
              break;
            }
          }
        },
        removeFromBox: (e, model) => {
          let index = model.index;

          if (model.product) {
            index = this._data.boxSlots
              .slice()
              .reverse()
              .findIndex(slot => slot.product && slot.product.handle === model.product.handle);
            let count = this._data.boxSlots.length - 1;
            index = index >= 0 ? count - index : index;
          }

          for (let [i, slot] of this._data.boxSlots.entries()) {
            if (i === index) {
              this._data.boxSlots.splice(i, 1, {
                product: null
              });
              break;
            }
          }
        },
        incrementQuantity: (e, model) => {
          this._data.quantity++;
        },
        decrementQuantity: (e, model) => {
          this._data.quantity--;
          if (this._data.quantity <= 0) {
            this._data.quantity = 1;
          }
        },
        quickViewProduct: async (e, model) => {
          e.preventDefault();

          this._quickViewPopupContent.innerHTML = '';
          this._data.quickViewOpen = true;
          this._data.quickViewLoading = true;

          let { data } = await axios.get(`/products/${model.product.handle}?view=quickview`);
          let parser = new DOMParser();
          let html = parser.parseFromString(data, 'text/html');
          let fetchedElem = html.querySelector('#shopify-section-ffc--product');
          this._quickViewPopupContent.innerHTML = fetchedElem.innerHTML;

          let form = this._quickViewPopupContent.querySelector('[data-add-to-box]');
          let quantityElem = form.querySelector('[data-quantity]');
          let freeSlots = this._data.limit - this._boxCount();
          if (freeSlots === 0) freeSlots = 1;

          quantityElem.setAttribute('max', freeSlots);

          this._theme.mountComponents(this._quickViewPopupContent);

          this._ctx.emit('ffc--lazy-load-update');

          this._disableBodyScroll(this._quickViewPopupScroll, {
            reserveScrollBarGap: true
          });

          form.addEventListener('submit', e => {
            e.preventDefault();
            let quantity = Number(quantityElem.value);
            let toAdd = quantity > freeSlots ? freeSlots : quantity;

            if (this._boxCount() >= this._data.limit) {
              this._closeQuickView();
              return false;
            }

            for (let i = 0; i < toAdd; i++) {
              for (let [j, slot] of this._data.boxSlots.entries()) {
                if (!slot || !slot.product) {
                  this._data.boxSlots.splice(j, 1, {
                    product: model.product
                  });
                  break;
                }
              }
            }

            this._closeQuickView();
          });

          this._data.quickViewLoading = false;
        },
        closeQuickView: async (e, model) => {
          e.preventDefault();
          this._closeQuickView();
        },
        closeQuickViewFromPopup: async (e, model) => {
          if (e.target === this._quickViewPopup) {
            this._closeQuickView();
          }
        },
        openMobileSummary: async (e, model) => {
          e.preventDefault();
          if (!e.target.hasAttribute('data-close-mobile-summary')) {
            this._data.mobileSummaryOpen = true;
            this._disableBodyScroll(this._mobileSummary, {
              reserveScrollBarGap: true
            });
          }
        },
        closeMobileSummary: async (e, model) => {
          e.preventDefault();
          this._data.mobileSummaryOpen = false;
          this._clearAllBodyScrollLocks();
        },
        addToCart: async (e, model) => {
          this._data.loading = true;
          this._data.errorMessage = '';

          let buyType = this._data.buyType;

          let condensedBoxSlots = [];

          for (let item of this._data.boxSlots) {
            if (condensedBoxSlots.find(oItem => oItem.product.handle === item.product.handle))
              continue;

            let matchingHandles = this._data.boxSlots.filter(
              oItem => oItem.product.handle === item.product.handle
            );

            condensedBoxSlots.push({
              product: item.product,
              quantity: matchingHandles.length
            });
          }

          let items = [];

          let boxId = Math.random()
            .toString(36)
            .substring(2, 7);

          for (let item of condensedBoxSlots) {
            let variant = item.product.variants[0];

            let atcObject = {
              id: `${variant.id}`,
              quantity: `${item.quantity * this._data.quantity}`,
              properties: {
                '_box-id': boxId,
                '_box-size': this._data.limit,
                '_box-base-quantity': item.quantity
              }
            };

            if (buyType === 'subscribe') {
              let selling_plan = variant.selling_plan_allocations[this._data.selectedPeriod];
              atcObject.selling_plan = `${selling_plan.selling_plan_id}`;
            }

            items.push(atcObject);
          }

          try {
            await AjaxApi.addToCart({
              items: items
            });
            window.location = '/cart';
          } catch (error) {
            let regex = /You can't add more (.+) to the cart./;
            let productName = error.response.data.description.match(regex);
            this._data.loading = false;
            this._data.errorMessage = `Error: ${error.response.data.description}`;
            console.log(error.response);
          }
        }
      }
    };
  }

  _closeQuickView() {
    let transitionEvent = e => {
      if (e.target != this._quickViewPopup) return;
      this._quickViewPopupContent.innerHTML = '';
      this._clearAllBodyScrollLocks();
      this._quickViewPopup.removeEventListener('transitionend', transitionEvent, false);
    };
    this._quickViewPopup.addEventListener('transitionend', transitionEvent, false);

    this._data.quickViewOpen = false;
    this._data.quickViewLoading = false;
  }

  _boxCount() {
    return this._data.boxSlots.filter(item => item && item.product).length;
  }
}

export default BoxBuilder;
