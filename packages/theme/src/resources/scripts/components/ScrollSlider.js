import Component from '../inherited/Component'

import '../vendor/greensock/TweenMax'
import '../vendor/greensock/utils/Draggable'
import '../vendor/greensock/plugins/ThrowPropsPlugin'

export default class ScrollSlider extends Component {
  constructor (elem, theme, options, ctx) {
    super(elem, theme, options, ctx)
    this._sendEvents = true
    this._rangeComponent = null
  }

  mount () {
    
    this._ctx.on('ffc--mounted', () => {
      if (this._options.rangeSlider) {
        this._rangeComponent = this._theme.getComponent(this._options.rangeSlider).component
        if (this._rangeComponent._isMounted) {
          this._rangeEvents()
        } else {
          this._rangeComponent._elem.addEventListener('range-slider--ready', e => {
            this._rangeEvents()
          })
        }
      } 
    })

    window.Draggable.create(this._elem, {
      type: "scrollLeft",
      throwProps: true
    });

    this._visibilityCheck()

    this._elem.addEventListener('scroll', e => {
      let maxScroll = this._elem.scrollWidth - this._elem.clientWidth
      let percent = this._elem.scrollLeft / maxScroll

      this._visibilityCheck()

      if (this._rangeComponent && this._sendEvents) {
        let max = this._rangeComponent._options.range.max[0]
        let newValue = max * percent
        newValue = Math.round(newValue)
        this._rangeComponent.set(newValue)
      }
      
    }, { passive: true })
  }

  _rangeEvents () {
    this._rangeComponent._rangeElem.noUiSlider.on('start', e => {
      this._sendEvents = false
    })

    this._rangeComponent._rangeElem.noUiSlider.on('end', e => {
      this._sendEvents = true
    })
  }

  _visibilityCheck () {
    let width = this._elem.clientWidth
    let children = this._elem.firstElementChild.children

    for (let child of children) {
      let offset = child.clientWidth / 2
      let min = this._elem.scrollLeft - offset
      let max = this._elem.scrollLeft + width + offset

      let childStart = child.offsetLeft
      let childEnd = childStart + child.clientWidth
      if (childStart >= min && childEnd <= max) {
        child.classList.add('active')
      } else {
        child.classList.remove('active')
      }
    }
  }

  scrollPercent (percent) {
    let maxScroll = this._elem.scrollWidth - this._elem.clientWidth
    let newScroll = percent * maxScroll
    this._elem.scrollTo(newScroll, 0)
  }
}