import Helpers from '../lib/Helpers';

export default class Component {
  constructor(elem, theme, options, ctx) {
    this._theme = theme;
    this._elem = elem;
    this._ctx = ctx;
    this._options = options;
    this._originalOptions = { ...this._options };

    this._addedResizeListener = false;
    this._optionsCheck(true);
  }

  _optionsCheck(firstRun) {
    if (Array.isArray(this._options)) return false;

    let newOptions = { ...this._originalOptions };
    newOptions.usingSettingsFor = 'desktop';

    if (this._originalOptions.responsive && this._originalOptions.responsive.length) {
      for (let optionsObj of this._originalOptions.responsive) {
        if (matchMedia(`only screen and (max-width: ${optionsObj.breakpoint})`).matches) {
          newOptions = { ...this._originalOptions, ...optionsObj.settings };
          newOptions.usingSettingsFor = 'tablet';
        }
      }
    }

    if (firstRun) {
      this._options = newOptions;
    } else if (this._options.usingSettingsFor !== newOptions.usingSettingsFor) {
      this._options = newOptions;
      return true;
    }

    return false;
  }

  reinitOnResize() {
    if (this._addedResizeListener) return;
    window.addEventListener(
      'resize',
      e => {
        let changed = this._optionsCheck();

        if (changed) {
          this.unmount();
          this.mount();

          Helpers.nextFrame(() => {
            if (typeof this.update === 'function') {
              this.update();
            }
          });
        }
      },
      { passive: true }
    );
    this._addedResizeListener = true;
  }
}
