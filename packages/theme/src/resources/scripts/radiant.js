__webpack_public_path__ = window.__webpack_public_path__;

import '../styles/ffc.scss';
import Theme from './Theme';

let theme = new Theme();
theme.mountComponents();

document.addEventListener('shopify:section:unload', e => {
  theme.unmountComponents(e.target);
});

document.addEventListener('shopify:section:load', e => {
  theme.mountComponents(e.target);
  theme._ctx.emit('ffc--lazy-load-update');
});
