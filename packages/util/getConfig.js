const fs = require('fs-extra')
const path = require('path')
const merge = require('merge-deep')
const exit = require('exit')

/**
 * internal modules
 */
const abs = require('./abs.js')
const fixPathSeparators = require("./fixPathSeparators.js")
const logger = require('./logger.js')
const log = logger('slater')

module.exports = function getConfig (options) {
  let configpath = abs(options.config || 'radiant.config.js')

  let scriptsPath = 'src/resources/scripts/radiant.js'

  if (!fs.existsSync(scriptsPath)) {
    scriptsPath = 'src/resources/scripts/ffc.js'
  }

  if (!fs.existsSync(configpath)) {
    configpath = abs('ffc.config.js')
  }

  if (!fs.existsSync(configpath)) {
    configpath = abs('slater.config.js')
  }

  if (!fs.existsSync(configpath)) {
    log.error(`looks like your config file (${options.config || 'ffc.config.js'}) is missing`)
    exit()
  }

  /**
   * deep merge user config with defaults
   */
  const config = merge({
    in: 'src/theme',
    out: 'dist/',
    assets: {
      in: scriptsPath,
    }
  }, require(configpath))

  config.assets.alias = {
    '@': abs(path.dirname(config.assets.in)),
    ...(config.assets.alias || {})
  }

  if (!config.assets.presets) {
    config.assets.presets = [
      'maps'
    ]
  }

  if (config.assets.presets && !config.assets.presets.includes('maps')) {
    config.assets.presets.push('maps')
  }

  if (!config.assets.out) {
    config.assets.out = fixPathSeparators(path.join(config.out, 'assets'))
  }

  /*
   * add reference to theme we're dealing with
   */
  config.themes = config.themes

  if (!config.themes.length) {
    log.error(`your ${options.theme} theme appears to be missing`)
    exit()
  }

  config.ignore = [].concat(config.ignore || [], [
    '**/scripts/**',
    '**/scripts',
    '**/styles/**',
    '**/styles',
    'DS_Store',
    '*.yml',
    '.DS_Store',
    'node_modules'
  ])

  for (let theme of config.themes) {
    theme.name = options.theme || 'development'
    theme.ignore = config.ignore
  }

  /**
   * overwrite paths to ensure they point to the cwd()
   */
  config.in = abs(config.in || './src')
  config.out = abs(config.out || './build')

  return config
}
