const fs = require('fs-extra')
const path = require('path')
const onExit = require('exit-hook')
const exit = require('exit')
const chokidar = require('chokidar')
const link = require('terminal-link')
/**
 * internal modules
 */
const compiler = require('@ffc-sync/compiler')
const sync = require('@ffc-sync/sync')
const {
  logger,
  match,
  sanitize,
  abs,
  fixPathSeparators
} = require('@ffc-sync/util')

const log = logger('ffc-sync')

function logStats (stats, opts = {}) {
  log.info('built', `in ${stats.duration}s\n${stats.assets.reduce((_, asset, i) => {
    const size = opts.watch ? '' : asset.size + 'kb'
    return _ += `  > ${log.colors.gray(asset.name)} ${size}${i !== stats.assets.length - 1 ? `\n` : ''}`
  }, '')}`)
}

/**
 * input absolute filepath, return
 *   - its filename (as a Shopify "key")
 *   - where it's coming from
 *   - where it's going
 *
 *   e.g. "/Users/user/Sites/projects/my-project/src/snippets/snip.liquid"
 *
 *   {
 *     filename: "snippets/snip.liquid",
 *     src: "/Users/user/Sites/projects/my-project/src/snippets/snip.liquid",
 *     dest: "/Users/user/Sites/projects/my-project/build/snippets/snip.liquid"
*    }
 */
function formatFile (filepath, src, dest) {
  if (!filepath) return {}

  const filename = sanitize(filepath)

  return {
    filename,
    src: filepath,
    dest: path.join(dest, filename)
  }
}

module.exports = function createApp (config) {
  return {
    copy () {
      return new Promise((res, rej) => {
        fs.emptyDir(config.out)
          .then(() => {
            fs.copy(config.in, config.out, {
              filter (src, dest) {
                return !match(src, config.ignore)
              }
            })
              .then(res)
              .catch(e => {
                log.error(e.message || e)
                rej(e)
                exit()
              })
          })
          .catch(e => {
            log.error(e.message || e)
            rej(e)
            exit()
          })
      })
    },
    build () {
      log.info(
        'building',
        `${config.themes[0].name} theme`
      )
      console.log('')

      return new Promise((res, rej) => {
        if (!fs.existsSync(abs(config.assets.in))) return

        const bundle = compiler(config.assets)

        bundle.on('error', e => {
          if(Array.isArray(e)) {
            for (let error of e) {
              let message = `${error.moduleName}:\n${error.message}`
              log.error(message)
            }
          } else {
            log.error(e.message)
          }
          rej(e)
        })
        bundle.on('stats', stats => {
          logStats(stats)
          res(stats)
        })

        return bundle.build()
      })
    },
    watch () {
      let themes = config.themes.filter(theme => !theme.deployOnly)

      for (let theme of themes) {
        log.info(
          'watch',
          link(
            `${theme.name} theme`,
            `https://${theme.store}/?fts=0&preview_theme_id=${theme.id}`
          )
        )
      }
      console.log('')

      /**
       * utilities for watch task only
       */
      function copyFile ({ filename, src, dest }) {
        return fs.copy(src, dest)
          .catch(e => {
            log.error(`copying ${filename} failed\n${e.message || e}`)
          })
      }
      function deleteFile ({ filename, src, dest }) {
        return fs.remove(dest)
          .catch(e => {
            log.error(`deleting ${filename} failed\n${e.message || e}`)
          })
      }
      function syncFile ({ theme, filename, src, dest }) {
        if (!filename) return Promise.resolve(true)

        return theme.sync(dest)
          .then(() => {
            log.info('synced', `${filename} to ${theme.config.store}`)
          })
          .catch(({ errors, key }) => {
            log.error(`syncing ${key} failed - ${errors?.asset ? errors.asset.join('  ') : JSON.stringify(errors)}`)
          })
      }
      function unsyncFile ({ theme, filename, src, dest }) {
        if (!filename) return Promise.resolve(true)

        return theme.unsync(dest)
          .then(() => {
            log.info('unsynced', filename)
          })
          .catch(({ errors, key }) => {
            log.error(`syncing ${key} failed - ${errors.asset ? errors.asset.join('  ') : JSON.stringify(errors)}`)
          })
      }

      // @see https://github.com/paulmillr/chokidar/issues/773
      const watchers = [
        chokidar.watch(config.in, {
          persistent: true,
          ignoreInitial: true,
          ignore: config.ignore
        })
          .on('add', file => {
            if (!file) return
            file = fixPathSeparators(file)
            if (match(file, config.ignore)) return
            copyFile(formatFile(file, config.in, config.out))
          })
          .on('change', file => {
            if (!file) return
            file = fixPathSeparators(file)
            if (match(file, config.ignore)) return
            copyFile(formatFile(file, config.in, config.out))
          })
          .on('unlink', file => {
            if (!file) return
            file = fixPathSeparators(file)
            if (match(file, config.ignore)) return
            deleteFile(formatFile(file, config.in, config.out))
          }),

        chokidar.watch(config.out, {
          ignore: /DS_Store/,
          persistent: true,
          ignoreInitial: true
        })
          .on('add', file => {
            if (!file) return false
            for (let theme of themes) {
              let params = formatFile(fixPathSeparators(file), config.in, config.out)
              params.theme = sync(theme)
              syncFile(params)
            }
          })
          .on('change', file => {
            if (!file) return false
            for (let theme of themes) {
              let params = formatFile(fixPathSeparators(file), config.in, config.out)
              params.theme = sync(theme)
              syncFile(params)
            }
          })
          .on('unlink', file => {
            if (!file) return false
            for (let theme of themes) {
              let params = formatFile(fixPathSeparators(file), config.in, config.out)
              params.theme = sync(theme)
              unsyncFile(params)
            }
          })
      ]

      onExit(() => {
        watchers.map(w => w.close())
      })

      if (fs.existsSync(abs(config.assets.in))) {
        const bundle = compiler(config.assets)

        bundle.on('error', e => {
          if(Array.isArray(e)) {
            for (let error of e) {
              let message = `${error.moduleName}:\n${error.message}`
              log.error(message)
            }
          } else {
            log.error(e.message)
          }
        })
        bundle.on('stats', stats => {
          logStats(stats, { watch: true })
        })

        bundle.watch()

        onExit(() => {
          bundle.close()
        })
      }
    }
  }
}
