# ffc-sync
Sync files between your local machine and a remote Shopify theme.

```bash
npm i ffc-sync -g
```
<br />

A new toolkit for building and deploying themes on Shopify. Built on top of slater

Example ffc.config.js:
```javascript
module.exports = {
  ignore: [ '**/config/settings_data.json', '**/templates/*.json' ],
  themes: [
    {
      id: 'xxx',
      password: 'xxx',
      store: 'xxx.myshopify.com'
    },
    {
      id: 'xxx',
      password: 'xxx',
      store: 'xxx.myshopify.com'
    },
    {
      id: 'xxx',
      password: 'xxx',
      store: 'xxx.myshopify.com'
    },
    {
      id: 'xxx',
      password: 'xxx',
      store: 'xxx.myshopify.com',
      deployOnly: true,
      code: "EU"
    },
    {
      id: 'xxx',
      password: 'xxx',
      store: 'xxx.myshopify.com',
      deployOnly: true,
      code: "US"
    },
  ]
}
```

Any themes with deployOnly: true will not sync with npm run dev, these will only be deployed to using the ffc-to-deploy.txt file. It's best to use this method when using a staging store since you only want to deploy certain changes to other stores.

package.json Scripts:
```json
"dev": "ffc-sync watch",
"build": "ffc-sync build",
"copySettings": "ffc-sync copySettings",
"copyLocales": "ffc-sync copyLocales",
"copyTemplates": "ffc-sync copyTemplates",
"copy": "ffc-sync copyAll",
"downloadTemplates": "ffc-sync downloadTemplates",
"syncAll": "ffc-sync syncAll",
"minify": "npm run build && ffc-sync syncJsCss",
"push": "git pull && git add . && git commit -m build && git push"
```

Example ffc-to-deploy.txt for multiple sites:
```
dist/layout/theme.liquid
dist/assets/ffc.js
dist/sections/example.liquid
```

Command to deploy list of files to all deployOnly themes:
```bash
ffc-sync deployFiles LIST
```

Command to deploy all files to all deployOnly themes (except settings_data.json and json templates):
```bash
ffc-sync deployFiles ALL
```

Command to deploy to specific country based on the config
```bash
ffc-sync deployFiles LIST US
```

Command to deploy all files to specific country based on the config
```bash
ffc-sync deployFiles ALL US
```

Command to copy settings from a theme to the dev theme, if no theme id is specified the live theme will be used
```bash
ffc-sync copySettings [theme id]
```

Command to copy locales from a theme to the dev theme, if no theme id is specified the live theme will be used
```bash
ffc-sync copyLocales [theme id]
```

Command to copy templates from a theme to the dev theme, if no theme id is specified the live theme will be used
```bash
ffc-sync copyTemplates [theme id]
```

Command to copy settings, locales and templates from a theme to the dev theme, if no theme id is specified the live theme will be used
```bash
ffc-sync copyAll [theme id]
```

Command to sync all files between two commits to the dev theme:
```bash
ffc-sync syncCommits [commit 1 hash] [commit 2 hash]
```