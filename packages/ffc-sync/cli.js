#! /usr/bin/env node
'use strict'

const fs = require('fs-extra')
const path = require('path')
const glob = require('glob')
const exit = require('exit')
const axios = require('axios')
const sync = require('@ffc-sync/sync')
const axiosThrottle = require('axios-request-throttle')

const { promisify } = require('util');
const exec = promisify(require('child_process').exec)

axiosThrottle.use(axios, { requestsPerSecond: 2 });

const {
  logger,
  getConfig
} = require('@ffc-sync/util')

const pkg = require('./package.json')
const createApp = require('./index.js')

const prog = require('commander')
  .version(pkg.version)
  .option('-c, --config <path>', 'specify the path to your config file')
  .option('-d, --debug', 'output some debugging logs')
  .option('-t, --theme <name>', 'specify a named theme from your config file')

const log = logger('ffc-sync')

prog
  .command('watch')
  .action(() => {
    const config = getConfig(prog)
    prog.debug && log.info('debug', JSON.stringify(config, null, '  '))
    const app = createApp(config)

    app.copy().then(app.watch)
  })

prog
  .command('build')
  .action(() => {
    const config = getConfig(prog)
    prog.debug && log.info('debug', JSON.stringify(config, null, '  '))
    const app = createApp(config)

    app.copy().then(app.build).then(() => exit())
  })


let copySettings = async (theme, themeFrom, axiosInstance) => {
  log.info('STARTING', theme.store)

  console.log('')

  let headers = {
    'X-Shopify-Access-Token': theme.password,
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }

  log.info("GETTING SETTINGS", theme.store)

  console.log('')

  log.progress('upload', 'copy settings', 0)

  let { data: themeData } = await axiosInstance.get(`https://${theme.store}/admin/api/2021-01/themes.json`)

  log.progress('upload', 'copy settings', 40)

  let { data: assetData } = await axiosInstance.get(`https://${theme.store}//admin/api/2021-01/themes/${themeFrom.id}/assets.json?asset[key]=config/settings_data.json`)

  log.progress('upload', 'copy settings', 70)

  let assetStr = assetData.asset.value

  await axiosInstance.put(`https://${theme.store}/admin/api/2021-01/themes/${theme.id}/assets.json`, {
    asset: {
      key: "config/settings_data.json",
      value: assetStr
    }
  })

  log.progress('upload', 'copy settings', 100)

  console.log('')

  log.info('COPIED SETTINGS TO DEV THEME', theme.store)

  console.log('')
}

let copyLocales = async (theme, themeFrom, axiosInstance) => {
  log.info("GETTING LOCALES", theme.store)

  console.log('')

  let { data: assetData } = await axiosInstance.get(`themes/${themeFrom.id}/assets.json`)

  let jsonTemplates = assetData.assets.filter(asset => asset.key.includes('locales/') && asset.key.endsWith('.json'))

  await Promise.all(jsonTemplates.map(async template => {
    let { data: assetData } = await axiosInstance.get(`themes/${themeFrom.id}/assets.json?asset[key]=${template.key}`)

    await axiosInstance.put(`themes/${theme.id}/assets.json`, {
      asset: {
        key: template.key,
        value: assetData.asset.value
      }
    })

    fs.writeFile(`src/theme/${template.key}`, assetData.asset.value, { flag: 'w+' }, err => {
      if (err) {
        console.error(err)
      }
    })

    log.info(`COPIED ${template.key} TO DEV THEME`, theme.store)

  }))

  log.info("ALL LOCALES COPIED TO DEV THEME & DOWNLOADED", theme.store)

  console.log('')
}

let copyTemplates = async (theme, themeFrom, axiosInstance) => {
  log.info("GETTING JSON TEMPLATES", theme.store)

  console.log('')

  let { data: assetData } = await axiosInstance.get(`themes/${themeFrom.id}/assets.json?assets.json`)

  let jsonTemplates = assetData.assets.filter(asset => asset.key.includes('templates/') && asset.key.endsWith('.json'))

  await Promise.all(jsonTemplates.map(async template => {

    try {
      let { data: assetData } = await axiosInstance.get(`themes/${themeFrom.id}/assets.json?asset[key]=${template.key}`)

      await axiosInstance.put(`themes/${theme.id}/assets.json`, {
        asset: {
          key: template.key,
          value: assetData.asset.value
        }
      })

      log.info('COPIED TO DEV THEME', template.key)

      console.log('')
    } catch (e) {
      console.error('ERROR ON FILE', template.key)
      console.error(e.message)
      console.error(e.response?.data);
    }

  }))

  log.info("ALL JSON TEMPLATES COPIED TO DEV THEME", theme.store)

  console.log('')
}

let copySectionGroups = async (theme, themeFrom, axiosInstance) => {
  log.info("GETTING JSON SECTION GROUPS", theme.store)

  console.log('')

  let { data: assetData } = await axiosInstance.get(`themes/${themeFrom.id}/assets.json?assets.json`)

  let jsonTemplates = assetData.assets.filter(asset => asset.key.includes('sections/') && asset.key.endsWith('.json'))

  await Promise.all(jsonTemplates.map(async template => {

    try {
      let { data: assetData } = await axiosInstance.get(`themes/${themeFrom.id}/assets.json?asset[key]=${template.key}`)
      await axiosInstance.put(`themes/${theme.id}/assets.json`, {
        asset: {
          key: template.key,
          value: assetData.asset.value
        }
      })

      log.info('COPIED TO DEV THEME', template.key)

      console.log('')
    } catch (e) {
      console.error('ERROR ON FILE', template.key)
      console.error(e.message)
      console.error(e.response?.data);
    }

  }))

  log.info("ALL JSON SECTION GROUPS COPIED TO DEV THEME", theme.store)

  console.log('')
}

let getActiveTheme = (themeData, themeId) => {
  let activeTheme = themeId ? themeData.themes.find(theme => `${theme.id}` === `${themeId.trim()}`) : themeData.themes.find(theme => theme.role === 'main')

  if (themeId) {
    log.info('Using specified theme id', themeId)
  } 

  return activeTheme
}

prog
  .command('copySettings')
  .arguments('[themeId]')
  .action(async (themeId) => {
    const config = getConfig(prog)
    prog.debug && log.info('debug', JSON.stringify(config, null, '  '))
    const app = createApp(config)

    try {
      let themes = config.themes.filter(theme => !theme.deployOnly)

      for (let theme of themes) {
        let axiosInstance = axios.create({
          baseURL: `https://${theme.store}/admin/api/2021-01/`,
          headers: {
            'X-Shopify-Access-Token': theme.password,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          }
        })
        axiosThrottle.use(axiosInstance, { requestsPerSecond: 2 });

        let { data: themeData } = await axiosInstance.get(`themes.json`)
        let activeTheme = getActiveTheme(themeData, themeId)

        await copySettings(theme, activeTheme, axiosInstance)
      }

    } catch(e) {
      console.error("ERROR")
      if (e.response) {
        console.error(e.response.data)
      } else {
        console.error(e)
      }
    }

    exit()
  })

prog
  .command('copyLocales')
  .arguments('[themeId]')
  .action(async (themeId) => {
    const config = getConfig(prog)
    prog.debug && log.info('debug', JSON.stringify(config, null, '  '))
    const app = createApp(config)

    try {
      let themes = config.themes.filter(theme => !theme.deployOnly)
      for (let theme of themes) {
        let axiosInstance = axios.create({
          baseURL: `https://${theme.store}/admin/api/2021-01/`,
          headers: {
            'X-Shopify-Access-Token': theme.password,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          }
        })
        axiosThrottle.use(axiosInstance, { requestsPerSecond: 2 });

        let { data: themeData } = await axiosInstance.get(`themes.json`)
        let activeTheme = getActiveTheme(themeData, themeId)

        await copyLocales(theme, activeTheme, axiosInstance)
      }

    } catch(e) {
      console.error("ERROR")
      if (e.response) {
        console.error(e.response.data)
      } else {
        console.error(e)
      }
    }

    exit()
  })

prog
  .command('copyTemplates')
  .arguments('[themeId]')
  .action(async (themeId) => {
    const config = getConfig(prog)
    prog.debug && log.info('debug', JSON.stringify(config, null, '  '))
    const app = createApp(config)

    try {
      let themes = config.themes.filter(theme => !theme.deployOnly)

      for (let theme of themes) {
        let axiosInstance = axios.create({
          baseURL: `https://${theme.store}/admin/api/2021-01/`,
          headers: {
            'X-Shopify-Access-Token': theme.password,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          }
        })
        axiosThrottle.use(axiosInstance, { requestsPerSecond: 2 });

        let { data: themeData } = await axiosInstance.get(`themes.json`)
        let activeTheme = getActiveTheme(themeData, themeId)

        await copyTemplates(theme, activeTheme, axiosInstance)
        await copySectionGroups(theme, activeTheme, axiosInstance)
        
      }

    } catch(e) {
      console.error("ERROR")
      if (e.response) {
        console.error(e.response.data)
      } else {
        console.error(e)
      }
    }

    exit()
  })

prog
  .command('copyAll')
  .arguments('[themeId]')
  .action(async (themeId) => {
    const config = getConfig(prog)
    prog.debug && log.info('debug', JSON.stringify(config, null, '  '))
    const app = createApp(config)

    try {
      let themes = config.themes.filter(theme => !theme.deployOnly)

      for (let theme of themes) {
        let axiosInstance = axios.create({
          baseURL: `https://${theme.store}/admin/api/2021-01/`,
          headers: {
            'X-Shopify-Access-Token': theme.password,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          }
        })
        axiosThrottle.use(axiosInstance, { requestsPerSecond: 2 });

        let { data: themeData } = await axiosInstance.get(`themes.json`)
        let activeTheme = getActiveTheme(themeData, themeId)

        await copySettings(theme, activeTheme, axiosInstance)
        await copyLocales(theme, activeTheme, axiosInstance)
        await copyTemplates(theme, activeTheme, axiosInstance)
        await copySectionGroups(theme, activeTheme, axiosInstance)
      }

    } catch(e) {
      console.error("ERROR")
      if (e.response) {
        console.error(e.response.data)
      } else {
        console.error(e)
      }
    }

    exit()
  })

prog
  .command('downloadTemplates')
  .action(async () => {
    const config = getConfig(prog)
    prog.debug && log.info('debug', JSON.stringify(config, null, '  '))
    const app = createApp(config)

    try {

      let themes = config.themes.filter(theme => !theme.deployOnly)
      for (let theme of themes) {

        let headers = {
          'X-Shopify-Access-Token': theme.password,
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }

        log.info("GETTING JSON TEMPLATES", theme.store)

        console.log('')

        let { data: assetData } = await axios.get(`https://${theme.store}//admin/api/2021-01/themes/${theme.id}/assets.json?assets.json`, {
          headers: headers
        })

        let jsonTemplates = assetData.assets.filter(asset => asset.key.includes('templates/') && asset.key.endsWith('.json'))

        let promises = []

        for (let template of jsonTemplates) {
          promises.push(new Promise(async (res, rej) => {
            let { data: assetData } = await axios.get(`https://${theme.store}//admin/api/2021-01/themes/${theme.id}/assets.json?asset[key]=${template.key}`, {
              headers: headers
            })

            fs.writeFile(`src/theme/${template.key}`, assetData.asset.value, { flag: 'w+' }, err => {
              if (err) {
                console.error(err)
                res()
                return
              }

              log.info("DOWNLOADED", template.key)

              console.log('')
            })

          }))
        }

        await Promise.all(promises)

        log.info("ALL JSON TEMPLATES DOWNLOADED", theme.store)

        console.log('')

        break
      }

    } catch(e) {
      console.error("ERROR")
      if (e.response) {
        console.error(e.response.data)
      } else {
        console.error(e)
      }
    }

    exit()
  })

prog
  .command('syncAll')
  .action(async () => {
    const config = getConfig(prog)
    prog.debug && log.info('debug', JSON.stringify(config, null, '  '))
    const app = createApp(config)

    try {

      let directory = 'dist/'
      directory = path.normalize(directory)

      var getDirectories = function (src, callback) {
        glob(src + '/**/*', { ignore: ["dist/assets", "dist/config", "dist/layout", "dist/locales", "dist/sections", "dist/snippets", "dist/templates", "dist/templates/customers", "dist/config/settings_data.json"]}, callback);
      };

      getDirectories('dist', async (err, files) => {
        if (err) {
          console.error('Error', err);
        } else {
          
          let themes = config.themes.filter(theme => !theme.deployOnly)
          for (let theme of themes) {
            log.info('SYNCING FILES', theme.store)

            console.log('')

            try {
              let syncTheme = sync(theme)
              await syncTheme.sync(files)
              log.info(`DEPLOYED FILES TO DEV THEME`, theme.store)

              console.log('')

            } catch(e) {
              console.error(theme.store)
              console.error(e)
            }
          }

        }

        exit()
      });
      

    } catch(e) {
      console.error("ERROR")
      if (e.response) {
        console.error(e.response.data)
      } else {
        console.error(e)
      }

      exit()
    }
    
  })

prog
  .command('syncJsCss')
  .action(async () => {
    const config = getConfig(prog)
    prog.debug && log.info('debug', JSON.stringify(config, null, '  '))
    const app = createApp(config)

    try {

      let directory = 'dist/assets/'
      directory = path.normalize(directory)

      let toSync = [
        'ffc.js',
        'ffc.css.liquid'
      ]

      if (fs.existsSync('dist/assets/radiant.js')) {
        toSync = [
          'radiant.js',
          'radiant.css.liquid'
        ]
      }

      let files = fs.readdirSync(directory);

      files.forEach(file => {
        if (file.includes('.chunk.js')) {
          toSync.push(file)
        }
      });

      toSync = toSync.map(item => {
        return path.normalize(`${directory}${item}`)
      })

      let themes = config.themes.filter(theme => !theme.deployOnly)

      for (let theme of themes) {
        log.info('SYNCING MINIFIED CSS/JS', theme.store)

        console.log('')

        try {
          let syncTheme = sync(theme)
          await syncTheme.sync(toSync)
          log.info(`DEPLOYED MINIFIED CSS/JS TO DEV THEME`, theme.store)

          console.log('')

        } catch(e) {
          console.error(theme.store)
          console.error(e)
        }
      }
      
    } catch(e) {
      console.error("ERROR")
      if (e.response) {
        console.error(e.response.data)
      } else {
        console.error(e)
      }
    }

    exit()
    
  })

prog
  .command('deployFiles')
  .arguments('[type] [country]')
  .action(async (type, country) => {
    const config = getConfig(prog)
    prog.debug && log.info('debug', JSON.stringify(config, null, '  '))
    const app = createApp(config)

    await app.copy()
    await app.build()

    try {

      let dir = './'
      dir = path.normalize(dir)
      let myArgs = process.argv.slice(2);

      let fileName = 'ffc-to-deploy.txt'
      let promises = []

      let themes = config.themes.filter(theme => theme.deployOnly)
      let contents = fs.readFileSync(fileName, 'utf8')
      let contentsArr = contents.split(/\r?\n/)

      contentsArr = contentsArr.filter(item => item.length > 0)
      contentsArr = contentsArr.map(item => {
        return path.normalize(`${dir}${item}`)
      })

      const keypress = async () => {
        process.stdin.setRawMode(true)
        return new Promise(resolve => process.stdin.once('data', data => {
          const byteArray = [...data]
          if (byteArray.length > 0 && byteArray[0] === 3) {
            console.log('^C')
            exit()
          }
          process.stdin.setRawMode(false)
          resolve()
        }))
      }

      if (country) {
        themes = themes.filter(theme => theme.code.toUpperCase() === country.toUpperCase())
      }

      if (contentsArr.find(item => item.includes('fullfat.js') || item.includes('ffc.js') || item.includes('radiant.js'))) {
        // Add chunks too
        let directory = path.normalize(dir + '/dist/assets')
        let files = fs.readdirSync(directory);

        files.forEach(file => {
          if (file.includes('.chunk.js')) {
            contentsArr.push(path.normalize(`${directory}/${file}`))
          }
        });
      }

      if (type === 'ALL') {
        await new Promise ((res, rej) => {
          let distDirectory = dir + '/dist'
          distDirectory = path.normalize(distDirectory)
          var getDirectories = function (src, callback) {
            glob(src + '/**/*', {}, callback);
          };
          getDirectories(distDirectory, async (err, files) => {
            files = files.filter(file => {
              let split = file.split('/')
              let valid = split[split.length - 1].includes('.') && !file.includes('settings_data.json') && !(file.includes('templates/') && file.endsWith('.json')) && !(file.includes('sections/') && file.endsWith('.json'))
              return valid
            })
            if (err) {
              console.log('Error', err);
            } else {
              contentsArr = files
              res(contentsArr)
            }
          });
        })
      }

      console.log('About to deploy:')

      console.log(' ')

      console.log(contentsArr)

      console.log(' ')

      console.log(`To: ${themes.map(theme => theme.code || theme.store).join()}`)

      console.log('Press any key to deploy, or Ctrl+C to exit')

      console.log(' ')

      await keypress()

      themes = themes.map(theme => {
        return {
          theme: sync(theme),
          store: theme.store
        }
      })

      for (let theme of themes) {
        log.info('Syncing', theme.store)
        promises.push(new Promise(async (res, rej) => {
          try {
            await theme.theme.sync(contentsArr)
            log.info('Deployed', theme.store)
            res()
          } catch(e) {
            console.log(theme.store)
            console.log(e)
            rej()
          }
        }))
      }

      await Promise.all(promises)
      
    } catch(e) {
      console.error("ERROR")
      if (e.response) {
        console.error(e.response.data)
      } else {
        console.error(e)
      }
    }

    exit()
    
  })

prog
  .command('listActiveThemes')
  .action(async (type, country) => {
    const config = getConfig(prog)
    prog.debug && log.info('debug', JSON.stringify(config, null, '  '))

    try {
      
      for (let theme of config.themes) {
        let { data } = await axios.get(`https://${theme.store}/admin/api/2021-01/themes.json`, {
          headers: {
            'X-Shopify-Access-Token': theme.password,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          }
        })

        let activeTheme = data.themes.find(theme => theme.role === 'main')

        log.info(theme.store, `- ${activeTheme.name} - ${activeTheme.id}`)
      }
      
    } catch(e) {
      console.error("ERROR")
      if (e.response) {
        console.error(e.response.data)
      } else {
        console.error(e)
      }
    }

    exit()
    
  })


prog
  .command('syncCommits')
  .argument('<commits...>')
  .action(async (commits) => {
    const config = getConfig(prog)
    prog.debug && log.info('debug', JSON.stringify(config, null, '  '))
    const app = createApp(config)

    await app.copy()
    await app.build()

    try {

      let directory = 'dist/assets/'
      directory = path.normalize(directory)

      let toSync = [
        'ffc.js',
        'ffc.css.liquid'
      ]

      if (fs.existsSync('dist/assets/radiant.js')) {
        toSync = [
          'radiant.js',
          'radiant.css.liquid'
        ]
      }

      let files = fs.readdirSync(directory);

      files.forEach(file => {
        if (file.includes('.chunk.js')) {
          toSync.push(file)
        }
      });

      toSync = toSync.map(item => {
        return path.normalize(`${directory}${item}`)
      })

      for (let commit of commits) {
        let filesOutput = await exec(`git show --stat=999 --oneline ${commit}`)

        filesOutput = filesOutput.stdout.trim().match(/\s(src.+\.\S+)/g)?.map(item => item.trim()) ?? []
        
        let validFiles = filesOutput.filter(item => item.includes('src/theme')).map(item => {
          return path.normalize(item.replace(/.*\/?src\/theme/, 'dist'))
        })

        toSync = [...toSync, ...validFiles]
      }

      toSync = toSync.filter(item => fs.existsSync(item))

      console.log('FILES TO SYNC:')
      console.log(toSync)

      let themes = config.themes.filter(theme => !theme.deployOnly)

      for (let theme of themes) {
        log.info('SYNCING FILES', theme.store)

        console.log('')

        try {
          let syncTheme = sync(theme)
          await syncTheme.sync(toSync)
          log.info(`DEPLOYED FILES TO DEV THEME`, theme.store)

          console.log('')

        } catch(e) {
          console.error(theme.store)
          console.error(e)
        }
      }

      
    } catch(e) {
      console.error("ERROR")
      if (e.response) {
        console.error(e.response.data)
      } else {
        console.error(e)
      }
    }

    exit()
    
  })

if (!process.argv.slice(2).length) {
  prog.outputHelp(txt => {
    console.log(txt)
    exit()
  })
} else {
  console.clear()
  log.info(`ffc-sync`, `v${pkg.version}\n`)
  prog.parse(process.argv)
}
