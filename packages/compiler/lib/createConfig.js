const path = require('path')
const clone = require('clone')
const webpack = require('webpack')

const ExtractCSS = require('mini-css-extract-plugin')
const TerserPlugin = require('terser-webpack-plugin');

const cwd = process.cwd()

const baseConfig = {
  output: {
    filename: '[name].js',
    chunkFilename: `[name].chunk.js?v=${Math.floor(Math.random() * 100000000000)}&enable_js_minification=1`
  },
  mode: 'development',
  target: 'web',
  performance: { hints: false },
  devtool: 'inline-cheap-module-source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: require.resolve('babel-loader'),
            options: {
              plugins: [
                require.resolve('@babel/plugin-syntax-dynamic-import'),
                require.resolve('@babel/plugin-transform-runtime')
              ],
              presets: [
                [
                  require.resolve('@babel/preset-env'),
                  {
                    "useBuiltIns": "usage",
                    "corejs": 3,
                    "targets": "> 0.25%, not dead, not ie <= 11"
                  }
                ]
              ]
            }
          }
        ]
      },
      // {
      //   test: /\.js$/,
      //   exclude: /(node_modules)/,
      //   use: {
      //       // Use `.swcrc` to configure swc
      //       loader: require.resolve("swc-loader"),
      //       options: {
      //         env: {
      //           targets: 'defaults, not IE 11',
      //           coreJs: "3.22",
      //           mode: 'usage'
      //         }
      //       }
      //   }
      // },
      {
        test: /\.(sa|sc)ss$/,
        exclude: /node_modules/,
        use: [
          {
            loader: require.resolve('file-loader'),
            options: {
                name: '[name].css.liquid'
            }
          },
          {
            loader: require.resolve('sass-loader'),
            options: {
              implementation: require('node-sass'),
              sassOptions: {
                outputStyle: "expanded",
                functions: {
                  'liquid($str)': function(str) {
                      return str
                  }
                }
              }
            }
          },
          {
            loader: require.resolve('postcss-loader'),
            options: {
              ident: 'postcss',
              plugins: [
                require('autoprefixer')({ flexbox: true }),
              ],
            }
          },
          {
            loader: require.resolve('sass-loader'),
            options: {
              sassOptions: { outputStyle: "expanded" },
              implementation: require('sass')
            }
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      '@': process.cwd()
    }
  },
  optimization: {
    chunkIds: 'natural',
    minimizer: [new TerserPlugin({
      extractComments: false,
    })],
    emitOnErrors: false
  },
  plugins: [
  ],
}

process.traceDeprecation = true;

module.exports = function createConfig (conf, watch) {
  const wc = clone(baseConfig)

  wc.entry = {
    [path.basename(conf.in, '.js')]: path.resolve(cwd, conf.in)
  }

  /**
   * merge output as an object,
   * or resolve a simple path
   */
  wc.output = Object.assign(
    wc.output,
    typeof conf.out === 'object' ? conf.out : {
      path: path.resolve(cwd, conf.out)
    }
  )

  wc.resolve.alias = Object.assign(wc.resolve.alias, conf.alias || {})

  wc.plugins = wc.plugins.concat([
    new webpack.DefinePlugin(conf.env || {}),
    new ExtractCSS({
      filename: '[name].css.liquid'
    })
  ].filter(Boolean))

  ;[].concat(conf.presets || [])
    .map(p => {
      const props = {
        config: wc,
        watch
      }

      try {
        typeof p === 'function' ? p(props) : (
          Array.isArray(p) ? (
            require(`../presets/${p[0]}.js`)(p[1])(props)
          ) : (
            require(`../presets/${p}.js`)()(props)
          )
        )
      } catch (e) {}
    })

  return wc
}
