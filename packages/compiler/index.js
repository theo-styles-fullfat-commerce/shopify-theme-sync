const fs = require('fs')
const path = require('path')
const webpack = require('webpack')

const createConfig = require('./lib/createConfig.js')
const formatStats = require('./lib/formatStats.js')

module.exports = conf => {
  let compiler

  const events = {}

  function emit (ev, ...data) {
    return (events[ev] || []).map(fn => {
      return fn(...data)
    })
  }

  function on (ev, fn) {
    events[ev] = (events[ev] || []).concat(fn)
    return () => events[ev].slice(events[ev].indexOf(fn), 1)
  }

  function closeServer () {
    emit('close')
  }

  return {
    on,
    close () {
      closeServer()
      return Promise.resolve(compiler ? new Promise(r => compiler.close(r)) : null)
    },
    build (options = {}) {
      emit('build')

      const config = createConfig(conf, false)
      config.mode = 'production'
      config.optimization.minimize = true

      return new Promise((res, rej) => {
        webpack(config).run((e, stats) => {
          if (e) {
            emit('error', e)
            rej(e)
            return
          }

          const s = formatStats(stats)

          if (s.errors && s.errors.length) emit('error', s.errors)
          if (s.warnings && s.warnings.length) emit('warn', s.warnings)

          emit('stats', s)
          emit('done', s)

          res(s)
        })
      })
    },
    watch (options = {}) {
      emit('watch')

      const config = createConfig(conf, true)

      compiler = webpack(config).watch(options, (e, stats) => {
        if (e) return emit('error', e.message)

        const s = formatStats(stats)

        if (s.errors && s.errors.length) {
           emit('error', s.errors)
        }
        if (s.warnings && s.warnings.length) emit('warn', s.warnings)

        emit('stats', s)
      })

      return {
        emit (ev) {}
      }
    }
  }
}
