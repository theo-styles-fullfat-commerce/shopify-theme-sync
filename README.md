# ffc-sync
Sync files between your local machine and a remote Shopify theme.

```bash
npm i ffc-sync -g
```
<br />

A new toolkit for building and deploying themes on Shopify. Built on top of slater

Example ffc.config.js:
```javascript
module.exports = {
  ignore: [ '**/config/settings_data.json', '**/templates/*.json' ],
  themes: [
    {
      id: 'xxx',
      password: 'xxx',
      store: 'xxx.myshopify.com'
    },
    {
      id: 'xxx',
      password: 'xxx',
      store: 'xxx.myshopify.com'
    },
    {
      id: 'xxx',
      password: 'xxx',
      store: 'xxx.myshopify.com'
    },
  ]
}
```

package.json Scripts:
```json
"dev": "ffc-sync watch",
"build": "ffc-sync build",
"copySettings": "ffc-sync copySettings",
"copyLocales": "ffc-sync copyLocales",
"copyTemplates": "ffc-sync copyTemplates",
"copy": "npm run copySettings && npm run copyLocales && npm run copyTemplates",
"downloadTemplates": "ffc-sync downloadTemplates",
"syncAll": "ffc-sync syncAll",
"minify": "npm run build && ffc-sync syncJsCss",
"push": "git pull && git add . && git commit -m build && git push"
```